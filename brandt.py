import matplotlib.pyplot as pyplot
import sys, os, math as m
import numpy as np 
import scipy as sp
from scipy.integrate import quad, dblquad

a0 = 0.2655
V = 0.404
# Ry = 13.6056
Ry = 27.21
step = 10000
r_max = 10

def Brandt(r, Z, N, lamb):
    return -Z/r + (Z-N)*np.exp(-r/lamb)/r

def BrandtVirial(r, Z, N, lamb):
    return -Z/(2*r) + (Z-N)*(np.exp(-r/lamb))/(2*r) - (Z-N)/(2*lamb)*(np.exp(-r/lamb))

def OrbitalPotential(l, r):
    return (l*(l+1))/(2*r*r)

def EffectivePotential(l, r, q):
    return -q/r + (l*(l+1))/(2*r*r)

def RadialHWf(r, n, l, q):
    kappa = q*r/n
    radial= {
        0:lambda r,q : (np.exp(-kappa/(2)))*(kappa/(2)-1)/np.sqrt(2),
        1:lambda r,q : (np.exp(-kappa/(2)))*kappa/(2)/np.sqrt(24)
    }
    return radial.get(l)(r,q)

def RadialHWf_der(r, n, l, q):
    return RadialHWf(r, n, l, q)

def FullEnergy(n, l, q):
    result = quad(lambda x:(Brandt(x, 6, 1, n, l, a0) + OrbitalPotential(l, x))*RadialHWf(x, n, l, q)*RadialHWf(x, n, l, q)*x*x, 0, np.inf)
    return result[0]

def GetEffectiveCharge(Z, n, l):
    residual = 100000000000
    q_effective = 0
    for x in range(0, Z*step, 1):
        current_q = x/step + 1/step
        current_residual = FullEnergy(n,l,current_q) + np.sqrt(current_q)/8
        if m.fabs(current_residual) < residual:
            residual = m.fabs(current_residual)
            q_effective = x/step
    print( "charge is", q_effective, "and residual is ", residual)
    print( "for n", n, "and l ", l)
    return q_effective, residual

# print("RadialHWf is ", RadialHWf(1, 2, 1, 1))
# print("FullEnergy 2s is ", FullEnergy(2, 0, 6))
# print( "FullEnergy 2p is ", FullEnergy(2, 1, 3.5657))


# two_s = GetEffectiveCharge(6,2,0)
# two_p = GetEffectiveCharge(6,2,1)
# print("FullEnergy 2s is ", FullEnergy(2, 0, two_s[0])*Ry)
# print( "FullEnergy 2p is ", FullEnergy(2, 1, two_p[0])*Ry)


def SphericalWF(r, E, l):
    k = np.sqrt(2*(V-E))
    radial= {
        0:lambda r : np.sin(k*r),
        1:lambda r : np.sin(k*r)/(k*r) - np.cos(k*r)
    }
    return radial.get(l)(r)

def EnergySpherical(E, l):
    Energy = quad(lambda x: BrandtVirial(x, 6, 1, 2,1,0.7)*pow(SphericalWF(x, E, l),2), 0, 0.7)
    # print (Energy[0])
    Norm = quad(lambda x: pow(SphericalWF(x, E, l), 2), 0, 0.7)
    print ('Full energy is ', Energy[0]/Norm[0], ' with E ', E)
    return (Energy[0]/Norm[0])

Potential = []
Potential2 = []
R = []
WF = []
for i in range (1, 1000):
    R.append(i*r_max/1000)
    # WF.append(RadialHWf(i*r_max/1000, 2, 1, 3.14)*9/16 + RadialHWf(i*r_max/1000, 2, 0, 3.22)/16)
    wave_f = (RadialHWf(i*r_max/1000, 2, 1, 3.14) + RadialHWf(i*r_max/1000, 2, 0, 3.22))/np.sqrt(2)
    # wave_f = (RadialHWf(i*r_max/1000, 2, 1, 3.14) + RadialHWf(i*r_max/1000, 2, 0, 3.22))

    WF.append(wave_f)

    # WF.append(RadialHWf(i*r_max/1000, 2, 1, 1)*(i*r_max/1000))
    # Potential.append((BrandtVirial(i*r_max/1000, 6, 1, a0) + OrbitalPotential(1, i*r_max/1000))*RadialHWf(i*r_max/1000, 2, 1, 3.14)*(i*r_max/1000)*RadialHWf(i*r_max/1000, 2, 1, 3.14)*(i*r_max/1000)*Ry)
    # Potential2.append((Brandt(i*r_max/1000, 6, 1, a0) + OrbitalPotential(1, i*r_max/1000))*RadialHWf(i*r_max/1000, 2, 1, 3.14)*(i*r_max/1000)*RadialHWf(i*r_max/1000, 2, 1, 3.14)*(i*r_max/1000)*Ry)
    Potential.append((BrandtVirial(i*r_max/1000, 6, 1, a0))*wave_f*(i*r_max/1000)*wave_f*(i*r_max/1000)*Ry)
    Potential2.append((Brandt(i*r_max/1000, 6, 1, a0))*wave_f*(i*r_max/1000)*wave_f*(i*r_max/1000)*Ry)

pyplot.plot(R, Potential, 'black')
# pyplot.plot(R, WF, 'black')
pyplot.plot(R, Potential2, 'red')
pyplot.show()
# for E in np.arange(0, V, 0.01):
#     l1 = []
#     l0 = []
#     X = []
#     l0.append(EnergySpherical(E, 0))
#     l1.append(EnergySpherical(E, 1))
#     X.append(E)

# pyplot.plot(X, l0, 'go', X, l1, 'ro')
# # pyplot.plot(X, l1)
# # pyplot.axis()
# pyplot.show()

# pyplot.plot([1,2,3,4], [1,2,3,4])
# pyplot.plot([1,2,3,4], [3,4,2,1])
# # pyplot.axis()
# pyplot.show()

# R = []
# WF = np.zeros(1000)
# for i in range(1000):
#     r = i*1.42/1000
#     R.append(r)
#     WF[i] = RadialHWf(r, 2,1,3.14)

# pyplot.plot(R, WF)
# pyplot.show()


