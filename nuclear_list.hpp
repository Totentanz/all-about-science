#pragma once

#include <iostream>
#include <math.h>
#include <complex>
#include <fstream>
#include <string>
#include <vector>
#include <map>

using namespace std;
vector<float> nuclei_coordinates;
map<string, vector<float> > nuclei;
vector<map<string, vector<float> > > nuclei_list = { {{"nuclei_coordinates" , {0.0, 0.0, 0.0}},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , {0.0,   1.42,    0.0}},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , {0.0,   4.26,    0.0}},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , {0.0,   5.68,    0.0}},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , {1.22975605,    -0.71,   0.0}},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , {1.22975605,    2.13,    0.0}},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , {1.22975605,    4.97,    0.0}},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , { 1.22975605,    6.39,    0.0}},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , { 2.4595121,     0.0,     0.0 }},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , { 2.4595121,     1.42,    0.0}},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , { 2.4595121,     4.26,    0.0}},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , { 2.4595121,     5.68,    0.0}},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , { 3.68926822,    -0.71,   0.0 }},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , { 3.68926822,    2.13,     0.0}},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , { 3.68926822,    4.97,     0.0}},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , { 3.68926822,     6.39,   0.0}},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , { 4.9190243, 0.0, 0.0 }},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , { 4.9190243, 1.42, 0.0}},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , { 4.9190243, 4.26, 0.0}},
                 {"charge", {6} }},
                 {{"nuclei_coordinates" , { 4.9190243, 5.68, 0.0}},
                 {"charge", {6} }}


};

