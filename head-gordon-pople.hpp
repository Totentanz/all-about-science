
#pragma once

#include <iostream>
#include <math.h>
#include <complex>
#include <fstream>
#include <string>
#include <vector>
#include <numeric>
#include <map>
#include <chrono>
#include <algorithm>
#include <tbb/task_group.h> 
#include "basis.hpp"

// float a_7 = 0.0;
// vector<float> r_7 = {};
// map<int, float> end_dict = {};

class HGP_Get_Value {
    public:
    float a_7_ = 0.0;
    vector<float> r_7_ = {};
    map<int, float> end_dict_ = {};
    HGP_Get_Value();
    float integrate( Basis_function& basis_i, Basis_function& basis_j, Basis_function& basis_k, Basis_function& basis_l);
    float hgp_begin_horizontal(Basis_function& g1, Basis_function& g2, Basis_function& g3, Basis_function& g4);
    float horizontal_recursion(int& r, Basis_function& g1, Basis_function& g2, Basis_function& g3, Basis_function& g4);
    float hgp_begin_vertical(int& m, Basis_function& g1, Basis_function& g2, Basis_function& g3, Basis_function& g4);
    float vertical_recursion(int& r, int& m, vector<Basis_function>& Basis_function_vector);
    vector<Basis_function> hgp_vertical_factory(int& r, Basis_function& g1, Basis_function& g2, Basis_function& g3, Basis_function& g4);
    float hgp_integrate( Basis_function& basis_i, Basis_function& basis_j, Basis_function& basis_k, Basis_function& basis_l);
};