/*
 * GSL_wrapper.c
 *
 *  Created on: 17 ���. 2019 �.
 *      Author: Admin
 */

#include "GSL_wrapper.h"

extern double normConstForF1;
extern double normConstForF2;
extern double normConstForW;

double _gsl_wrap_W_radial_inner (double x, void* params)
{
	gsl_params_W_radial* p = (gsl_params_W_radial*) params;
	return pow(W_radial(x, p->nu, p->n, p->n1, p->n2, p->m), 2)*EnFull(x, p->nu, 6, 0.2655)*(x+p->nu)/4;
}
double _gsl_wrap_W_radial_outer (double x, void* params)
{
	double result_qag;
	double result_qagiu;
	double error;
	gsl_params_W_radial* p = (gsl_params_W_radial*) params;
	p->nu = x;
//	printf("nu is: %.02f\n", x);
	gsl_integration_workspace *w_qag =  gsl_integration_workspace_alloc(20);
	gsl_integration_workspace *w_qagiu =  gsl_integration_workspace_alloc(20);

	gsl_integration_qag (&gsl_wrap_W_radial_inner, 0.0, 5.0, 0, 1e-7,20,GSL_INTEG_GAUSS21,w_qag, &result_qag, &error);
	gsl_integration_qagiu (&gsl_wrap_W_radial_inner, 5.0, 0, 1e-7,20,w_qagiu, &result_qagiu, &error);

	gsl_integration_workspace_free (w_qag);
	gsl_integration_workspace_free (w_qagiu);

	return result_qag+result_qagiu;
}

double IntegralOverW (int n, int n1, int n2, int m)
{
	double result_qag;
	double result_qagiu;
	double error;
	normConstForF1 = GetNormConstantForF(n1, m);
	normConstForF2 = GetNormConstantForF(n2, m);
	normConstForW = GetNormConstantForW(n);

//	printf("n1 is:\t%.02f\n n2 is:\t%.02f\n m is:\t%.02f\n", normConstForF1, normConstForF2, normConstForW);

	gsl_params_W_radial p = {n, n1, n2, m, 0};
	gsl_wrap_W_radial_outer.function = _gsl_wrap_W_radial_outer;
	gsl_wrap_W_radial_outer.params = (void*)&p;
	gsl_wrap_W_radial_inner.params = (void*)&p;
	gsl_wrap_W_radial_inner.function = _gsl_wrap_W_radial_inner;
	gsl_integration_workspace *w_qag =  gsl_integration_workspace_alloc(20);
	gsl_integration_workspace *w_qagiu =  gsl_integration_workspace_alloc(20);

	gsl_integration_qag (&gsl_wrap_W_radial_outer, 0.0, 5.0, 0, 1e-7,20,GSL_INTEG_GAUSS21, w_qag, &result_qag, &error);
	gsl_integration_qagiu (&gsl_wrap_W_radial_outer, 5.0, 0, 1e-7,20,w_qagiu, &result_qagiu, &error);

	gsl_integration_workspace_free (w_qag);
	gsl_integration_workspace_free (w_qagiu);

	return result_qag+result_qagiu;
}

extern double _gsl_wrap_Full_energy (double x, void* params) {
	model_parameters *p = (model_parameters*) params;
	double radial = RadialWaveFunction(x, p->n, p->l, p->q);
	double result = PotentialBrandtFull(x, p->Z, p->ionization, p->lambda)*radial*radial*x*x;
	return result;
}

double _gsl_wrap_Qeffective (double x, void* params) {
	model_parameters *p = (model_parameters*) params;
	return ChargeDistribution(x, p->Z, p->ionization, p->lambda);
}

extern double _gsl_wrap_Residual (double x, void* params) {
	model_parameters *p = (model_parameters*) params;
	// printf("x is %.03f\n", x);
	double radial = RadialWaveFunction(x, p->n, p->l, p->q);
	// printf("RadialWaveFunction is %.03f\n", radial);
	double potential = PotentialBrandtFull(x, p->Z, p->ionization, p->lambda);
	// printf("PotentialBrandtFull is %.03f\n", potential);
	double coulomb = p->q/x;
	// printf("coulomb is %.03f\n", coulomb);
	double result = (potential-coulomb + (p->l*(p->l+1))/(2*x*x))*radial*radial*x*x; // - coulomb;
	// printf("result is %.03f\n", result);
	return result;
}

extern double FullEnergyIntegral (model_parameters* p)
{
	double result = 0;
	double error;
	gsl_wrap_Full_energy.function = _gsl_wrap_Full_energy;
	gsl_wrap_Full_energy.params = (void*) p;

	gsl_integration_workspace *w_qagiu =  gsl_integration_workspace_alloc(20);
	gsl_integration_qagiu (&gsl_wrap_Full_energy, 0.0, 0, 1e-7,20,w_qagiu, &result, &error);
	gsl_integration_workspace_free (w_qagiu);

	return result;
}

extern double ResidualIntegral (model_parameters* p)
{
	double result = 0;
	double error;
	gsl_wrap_Residual.function = _gsl_wrap_Residual;
	gsl_wrap_Residual.params = (void*) p;
	// printf("l is: %i\n",p->l);

	gsl_integration_workspace *w_qagiu =  gsl_integration_workspace_alloc(20);
	gsl_integration_qagiu (&gsl_wrap_Residual, 0.0, 0, 1e-7,20,w_qagiu, &result, &error);
	// gsl_integration_qag (&gsl_wrap_Residual, 0.0, 1.34, 0, 1e-7,20,GSL_INTEG_GAUSS21,w_qagiu, &result, &error);
	gsl_integration_workspace_free (w_qagiu);

	return result;
}

extern double GetQEffectiveBrandt (model_parameters* p)
{
	double result = 0;
	double error;
	gsl_wrap_Qeffective.function = _gsl_wrap_Qeffective;
	gsl_wrap_Qeffective.params = (void*) p;
	gsl_integration_workspace *w_qagiu =  gsl_integration_workspace_alloc(20);
	gsl_integration_qag (&gsl_wrap_Qeffective, 0.0, p->lambda, 0, 1e-7,20,GSL_INTEG_GAUSS21,w_qagiu, &result, &error);
	gsl_integration_workspace_free (w_qagiu);
	printf("result is %.03f\n", result);

	return p->Z - result;
}

extern double GetExternalShielding (model_parameters* p)
{
	double result = 0;
	double error;
	gsl_wrap_Qeffective.function = _gsl_wrap_Qeffective;
	gsl_wrap_Qeffective.params = (void*) p;

	gsl_integration_workspace *w_qagiu =  gsl_integration_workspace_alloc(20);
	gsl_integration_qagiu (&gsl_wrap_Qeffective, p->lambda, 0, 1e-1,20,w_qagiu, &result, &error);
	gsl_integration_workspace_free (w_qagiu);
	printf("result GetExternalShielding is %.03f\n", result);
	return result;
}
extern double BKPotetialfromPrinc (double r, model_parameters* p)
{
	return GetExternalShielding(p) - GetQEffectiveBrandt(p)/r;
}