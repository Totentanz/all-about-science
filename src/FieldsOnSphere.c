#include "FieldsOnSphere.h"

double UesToIntegrate(double r, double q, double radius)
{

	double Rs = RadialWaveFunction(r, 2, 0, q);
	double Rp = RadialWaveFunction(r, 2, 1, q);
   	double xi = q*r;
	return (Rs*Rs + Rp*Rp)*xi*(radius - xi);
}

double R2sR2pXi3(double r, double q)
{
	double Rs = RadialWaveFunction(r, 2, 0, q);
	double Rp = RadialWaveFunction(r, 2, 1, q);
   	double xi = q*r;
	return (Rs*Rp)*xi*(xi)*(xi);
}

double R2sR2pXi(double r, double q)
{
	double Rs = RadialWaveFunction(r, 2, 0, q);
	double Rp = RadialWaveFunction(r, 2, 1, q);
   	double xi = q*r;
	return (Rs*Rp)*xi;
}