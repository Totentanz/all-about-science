#include "functions.h"


float a0 = 0.5291772;
double s = 2.6838026838;
#define CaliberFor2s  (2/(pow((2*1),(3/2))))
#define CaliberFor2p (2/sqrt(3*(pow((2*1),3))))

double RadialWaveFunction(double r, int n, int l, double q)
{
	double result = 0;
	double xi = (q*r);
    switch (n) {
		case 2:
        switch (l){
            case 0:
            result = (exp(-(xi)/(2)))*(xi/(2)-1)/sqrt(2);
            break;
            case 1:
            result = (exp(-xi/(2)))*xi/(2)/sqrt(24);
			            // result = pow(q, 5/2)*(exp(-xi/(2)))*xi*xi/(4)/sqrt(24);
			break;
        }
		break;
		case 1:
			result = 2*exp(-xi);
			break;
    }
    return result;
}

double PotentialBrandtFull(double r, int Z, int ionization, double lambda)
{
	return -(ionization/(2*r) + (Z-ionization)*(exp(-r/lambda))/(2*r) - (Z-ionization)/(2*lambda)*(exp(-r/lambda)));
}

double PotentialBrandtMain(double r, int Z, int ionization, double lambda)
{
	double result = 0;
	result = ionization/r + ((Z-ionization)/r)*exp(-r/lambda);
	return -result;
}

double EffectivePotential(int l, double r, double q)
{
	double result = 0;
	result = -q/r + (l*(l+1))/(2*r*r);
	return result;
}

//static double UedToIntegrateFirst(double r, parameters* p)
//{
//	parameters R20 = {
//			.q = p->q,
//			.n = 2,
//			.l = 0
//	};
//	parameters R21 = {
//			.q = p->q,
//			.n = 2,
//			.l = 1
//	};
//	float Rs = RadialWaveFunction(r, &R20);
//	float Rp = RadialWaveFunction(r, &R21);
//   	float xi = (2*p->q*r)/(2);
//	return (Rs*Rp)*xi*(xi)*(xi);
//}
//
//static double UedToIntegrateSecond(double r, parameters* p)
//{
//	parameters R20 = {
//			.q = p->q,
//			.n = 2,
//			.l = 0
//	};
//	parameters R21 = {
//			.q = p->q,
//			.n = 2,
//			.l = 1
//	};
//	float Rs = RadialWaveFunction(r, &R20);
//	float Rp = RadialWaveFunction(r, &R21);
//   	float xi = (2*p->q*r)/(2);
//	return (Rs*Rp);
//}
//
//extern double Ued (double r, double q, double lambda, double beta, double theta)
//{
//	 double  error;
//	 double result1, result2;
////	 gsl_sf_result result_of_Y;
//	 parameters Ued_p = {
//			 .q = q,
//			 .lambda = lambda
//	 };
//	 gsl_function Ued_First = {
//			 .params = &Ued_p,
//			 .function = &UedToIntegrateFirst
//	 };
//	 gsl_function Ued_Second = {
//			 .params = &Ued_p,
//			 .function = &UedToIntegrateSecond
//	 };
//
//	gsl_integration_workspace *w1 =  gsl_integration_workspace_alloc(20);
//	gsl_integration_workspace *w2 =  gsl_integration_workspace_alloc(40);
//
//	gsl_integration_qag (&Ued_First, 0.0, r/lambda, 0,1e-7,20, 1, w1, &result1, &error);
//	gsl_integration_qagiu (&Ued_Second, r/lambda,0,1e-7,40,w2, &result2, &error);
//	double Y01 = gsl_sf_legendre_sphPlm(1,0,theta);
///*
//	printf ("result = % .5f\n", result1);
//	printf ("result = % .5f\n", result2);
//	printf ("result = % .5f\n", result1+result2);
//	printf ("estimated error = % .18f\n", error);
//	printf ("intervals for 1 = %zu\n", w1->size);
//	printf ("intervals for 2 = %zu\n", w2->size);
//	*/
//	double sin_val = sin(2*beta);
//	sin_val /= (r*r*r/lambda/lambda);
//	gsl_integration_workspace_free (w1);
//	gsl_integration_workspace_free (w2);
//	return (result1+result2)*sin_val*Y01;
//}
//
//extern double Uek (double r, double q, double lambda, double beta, double theta)
//{
//	 double  error;
//	 double result1, result2;
//	 parameters Ued_p = {
//			 .q = q,
//			 .lambda = lambda
//	 };
//	 gsl_function Ued_First = {
//			 .params = &Ued_p,
//			 .function = &UedToIntegrateFirst
//	 };
//	 gsl_function Ued_Second = {
//			 .params = &Ued_p,
//			 .function = &UedToIntegrateSecond
//	 };
//
//	gsl_integration_workspace *w1 =  gsl_integration_workspace_alloc(20);
//	gsl_integration_workspace *w2 =  gsl_integration_workspace_alloc(40);
//
//	gsl_integration_qag (&Ued_First, 0.0, r/lambda, 0,1e-7,20, 1, w1, &result1, &error);
//	gsl_integration_qagiu (&Ued_Second, r/lambda,0,1e-7,40,w2, &result2, &error);
//	double Y11 = gsl_sf_legendre_sphPlm(1,1,theta);
///*
//	printf ("result = % .5f\n", result1);
//	printf ("result = % .5f\n", result2);
//	printf ("result = % .5f\n", result1+result2);
//	printf ("estimated error = % .18f\n", error);
//	printf ("intervals for 1 = %zu\n", w1->size);
//	printf ("intervals for 2 = %zu\n", w2->size);
//	*/
//	double sin_val = sin(2*beta);
//	sin_val /= (r*r*r/lambda/lambda);
//	gsl_integration_workspace_free (w1);
//	gsl_integration_workspace_free (w2);
//	return (result1+result2)*sin_val*Y11;
//}
//
//double Ues (double r, double q, double lambda, double beta)
//{
//	 double  error;
//	 double result;
//	 parameters Ues_p = {
//			 .q = q,
//			 .lambda = lambda
//	 };
//	 gsl_function Ues = {
//			 .params = &Ues_p,
//			 .function = &UesToIntegrate
//	 };
//
//	gsl_integration_workspace *w =  gsl_integration_workspace_alloc(20);
//
//	gsl_integration_qagiu (&Ues, r/lambda,0,1e-5,20,w, &result, &error);
//
//
//////	printf ("estimated error = % .18f\n", error);
////	printf ("intervals = %zu\n", w->size);
////	result *= (sin(2*beta)/(r*r*r*/(lambda*lambda)));
//	gsl_integration_workspace_free (w);
//	return result;
//}

double FullEnergy(double r, int Z, int N, int l, double q, double lambda)
{
	double R = RadialWaveFunction(r, 2, l, q);
	double xi = (q*r);
	return (N/(2*r) + (Z-N)*(exp(-r/lambda))/(2*r) + (Z-N)/(2*lambda)*(exp(-r/lambda))-q/r)*R*xi*R*xi;
}

static double FunctionToGetSolution (double r, model_parameters *p)
{
	return FullEnergy(r, p->Z, p->ionization, p->l, p->q, p->lambda);
}

double Variational_q (double q, int l)
{
	double result;
	double error;
	model_parameters param = {
			.q = q,
			.Z = 6,
			.n = 2,
			.l = l,
			.ionization = 1,
			.lambda = 0.2655
	};

	gsl_function FullEn = {
			.params = &param,
			.function = &FunctionToGetSolution
	};

	gsl_integration_workspace *w =  gsl_integration_workspace_alloc(20);

	gsl_integration_qagiu (&FullEn, 0.0, 0, 1e-5,20,w, &result, &error);


	gsl_integration_workspace_free (w);
	return result;
}


double PotentialOfTwoIons (double r, int Z, int ionization, double lambda) {
	return PotentialBrandtFull(r,Z,ionization,lambda) + PotentialBrandtFull(s - r,Z,ionization,lambda);
}

double ChargeDistribution(double r, int Z, int ionization, double lambda) {
	return (Z-ionization)*exp(-r/lambda)/lambda/lambda;
}