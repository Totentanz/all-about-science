/*
 * ParabolicSystem.c
 *
 *  Created on: 16 ���. 2019 �.
 *      Author: Admin
 */

#include "ParabolicSystem.h"

static int factorial(int x)
{
	int result = 1;
	for (int i = 1; i<=x; i++)
	{
		result *= i;
	}
	return result;
}

static double HyperGeometricFunction(int a, int b, double z, int iter) {
	double result = 0;
	for (int i = 0; i< iter; i++)
	{
		result+= pow(z, i)*a/(b*factorial(i));
	}

	return result;
}

double GetNormConstantForF(int n, int m)
{
	double result = 0;
	double n_fact = (double) factorial(n);
	double m_fact = (double) factorial(m);
	double n_m_fact = (double) factorial(n-m);
	result = (1/m_fact)*sqrt(n_m_fact/n_fact);
	return result;
}

double GetNormConstantForW(int n)
{
	double result = 0;
	result = sqrt(2)/(n*n);
	return result;
}

double F_helper (int n, int m, double rho)
{
	double result = 0;
//    printf("rho is:\t%.03f\n", rho);

    if (rho > 100) rho = 100;
	result = gsl_sf_hyperg_1F1_int(-n, m+1, rho)*pow(rho, m+1)*exp(-rho/2.0);
//	printf("f_helper is:\t%.03f\n", result);
	return result;
}
double W_radial(double xi, double nu, int n, int n1, int n2, int m)
{
	double result = 0;
	double xi_ = xi/(double)n;
	double nu_ = nu/(double)n;
//	printf("xi is:\t%.04f\n", xi);
	result = normConstForW*normConstForF1*normConstForF2*F_helper(n1, m, xi_)*F_helper(n1, m, nu_);
//	result = F_helper(n1, m, xi_)*F_helper(n1, m, nu_);
//	printf("result is: %.04f\n",result);
	return result;
}

double EnBrandt (double xi, double nu, int Z, double lambda)
{
	return 2/(xi+nu) + 2*(Z-1)*exp(-(xi+nu)/lambda)/(xi+nu);
}

double EnFull (double xi, double nu, int Z, double lambda)
{
	return 1/(xi+nu) + (Z-1)*exp(-(xi+nu)/lambda)/(xi+nu) - (Z-1)*exp(-(xi+nu)/lambda)/(2*lambda);
}

double Coulomb (double xi, double nu)
{
	return 2/(xi+nu);
}
