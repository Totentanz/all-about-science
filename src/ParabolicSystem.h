/*
 * ParabolicSystem.h
 *
 *  Created on: 16 ���. 2019 �.
 *      Author: Admin
 */

#ifndef SRC_PARABOLICSYSTEM_H_
#define SRC_PARABOLICSYSTEM_H_

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_math.h>

double normConstForF1;
double normConstForF2;
double normConstForW;
double GetNormConstantForF(int n, int m);
double GetNormConstantForW(int n);
double F_helper (int n, int m, double rho);
double W_radial(double xi, double nu, int n, int n1, int n2, int m);
double EnBrandt (double xi, double nu, int Z, double lambda);
double EnFull (double xi, double nu, int Z, double lambda);
double Coulomb (double xi, double nu);



#endif /* SRC_PARABOLICSYSTEM_H_ */
