#ifndef SRC_FUNCTIONS_H_
#define SRC_FUNCTIONS_H_

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_math.h>



typedef struct {
		int Z;
		double lambda;
		int ionization;
		double n;
		int l;
		double q;
} model_parameters;

extern double RadialWaveFunction(double r, int n, int l, double q);
extern double PotentialBrandtFull(double r,  int Z, int ionization, double lambda);
extern double FullEnergy(double r, int Z, int N, int l, double q, double lambda);
extern double PotentialBrandtMain(double r,  int Z, int ionization, double lambda);
extern double EffectivePotential(int l, double r, double q);
extern double IntegrationFunction(double x, void *p);
extern double IntegrationFunctionForX1(double x, void *p);
extern double IntegrationErrorFunction(float x);
extern double FunctionToIntegrate (double x, void * p);
//extern double Ues (double r, double q, double lambda, double beta);
//extern double Ued (double r, double q, double lambda, double beta, double theta);
//extern double Uek (double r, double q, double lambda, double beta, double theta);
extern double Variational_q (double q, int l);
extern double PotentialOfTwoIons (double r, int Z, int ionization, double lambda);
extern double ChargeDistribution(double r, int Z, int ionization, double lambda);




#endif /* SRC_FUNCTIONS_H_ */
