/*
 * GSL_wrapper.h
 *
 *  Created on: 17 ���. 2019 �.
 *      Author: Admin
 */

#ifndef SRC_GSL_WRAPPER_H_
#define SRC_GSL_WRAPPER_H_

#include "ParabolicSystem.h"
#include "functions.h"

typedef struct _gsl_params_W_radial {
	int n;
	int n1;
	int n2;
	int m;
	double nu;
}gsl_params_W_radial;

double IntegralOverW (int n, int n1, int n2, int m);
double FullEnergyIntegral (model_parameters* p);
double ResidualIntegral (model_parameters* p);
double GetQEffectiveBrandt(model_parameters* p);
extern double BKPotetialfromPrinc (double r, model_parameters* p);

double _gsl_wrap_W_radial_inner (double x, void* params);
double _gsl_wrap_W_radial_outer (double x, void* params);
double _gsl_wrap_Full_energy (double x, void* params);
double _gsl_wrap_Residual (double x, void* params);
double _gsl_wrap_Qeffective (double x, void* params);


gsl_params_W_radial gsl_params_W_radial_inner;
gsl_params_W_radial gsl_params_W_radial_outer;

gsl_function gsl_wrap_W_radial_inner;
gsl_function gsl_wrap_W_radial_outer;
gsl_function gsl_wrap_Full_energy;
gsl_function gsl_wrap_Residual;
gsl_function gsl_wrap_Qeffective;






#endif /* SRC_GSL_WRAPPER_H_ */
