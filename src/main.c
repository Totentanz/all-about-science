#include "GSL_wrapper.h"
#include <gsl/gsl_integration.h>

double step = 0.0001;
double rcurrent = 0.1;
double result = 0;
double tmpResult = 0;
double currentCoord = 0.1;
double potential = 0;
const double lambda = 0.2655;
const double fullRange = 5.25;
const double energyConv = 27.21;

model_parameters pFullEnergy = {
		.Z = 6,
		.n = 2,
		.l = 0,
		.q = 3.14,
		.lambda = 0.2655,
		.ionization = 1
};

void GetXAxis(int numberOfSteps)
{
	FILE* fd = fopen("X_axis.txt", "w");
	double step = fullRange/numberOfSteps;
	double tmpStep = 0;
	for (int i = 0; i<numberOfSteps; i++)
	{
		tmpStep+=step;
		fprintf(fd,"%.05f\n", tmpStep);
	}
	fclose(fd);
}

void GetInhomogeneousDistribution(char* filename, double currentLambda, int numberOfSteps)
{
	FILE* fd = fopen(filename, "w");
	double step = fullRange/numberOfSteps;
	double tmpStep = 0;
	double result = 0;
	for (int i = 0; i<numberOfSteps; i++)
	{
		tmpStep+=step;
		fprintf(fd,"%.05f, ", tmpStep);
		result = PotentialBrandtFull(tmpStep, 6,1,currentLambda)*energyConv;
		fprintf(fd,"%.05f\n", result);
	}
	fclose(fd);
}

void GetInhomogeneousDistributionWitnEff(char* filename, double currentLambda, int numberOfSteps, double qEff, int l)
{
	FILE* fd = fopen(filename, "w");
	double step = fullRange/numberOfSteps;
	double tmpStep = 0;
	double result = 0;
	double resultEff = 0;
	for (int i = 0; i<numberOfSteps; i++)
	{
		tmpStep+=step;
		fprintf(fd,"%.05f, ", tmpStep);
		result = PotentialBrandtFull(tmpStep, 6,1,currentLambda)*energyConv;
		resultEff = EffectivePotential(l, tmpStep, qEff);
		fprintf(fd,"%.05f, %.05f\n", result, resultEff);
	}
	fclose(fd);
}

void GetTwoFieldsDistribution(char* filename, double currentLambda, int numberOfSteps)
{
	FILE* fd = fopen(filename, "w");
	double step = fullRange/numberOfSteps;
	double tmpStep = 0.3;
	double result = 0;
	for (int i = 0; i<numberOfSteps; i++)
	{
		tmpStep+=step;
		result = PotentialOfTwoIons(tmpStep, 6,1,currentLambda);
		fprintf(fd,"%.03f, %.05f\n",tmpStep, result);
	}
	fclose(fd);
}

void GetBKPotetialfromPrinc(char* filename, model_parameters* p, int numberOfSteps)
{
	FILE* fd = fopen(filename, "w");
	double step = fullRange/numberOfSteps;
	double tmpStep = 0.2;
	double result = 0;
	double result_BK = 0;
	for (int i = 0; i<numberOfSteps; i++)
	{
		tmpStep+=step;
		result = -PotentialBrandtMain(tmpStep, 6,1,p->lambda);
		result_BK = -BKPotetialfromPrinc(tmpStep, p);
		fprintf(fd,"%.03f, %.05f, %.05f\n",tmpStep, result, result_BK);
	}
	fclose(fd);
}

void GetHomogeneousDistribution(char* filename, double currentLambda, int numberOfSteps)
{
	FILE* fd = fopen(filename, "w");
	double step = fullRange/numberOfSteps;
	double tmpStep = 0;
	double result = 0;
	for (int i = 0; i<numberOfSteps; i++)
	{
		tmpStep+=step;
		result = PotentialBrandtMain(tmpStep, 6,1,currentLambda);
		fprintf(fd,"%.05f\n", -result);
	}
	fclose(fd);
}

static void GetExtremalPoints() {
	int sign  = 1;
	FILE* fd = fopen("res_n1.txt", "w");
	FILE* fx = fopen("res_n0.txt", "w");
	for (int i = 0; i<2000; i++)
	{
		currentCoord+=step;
		tmpResult = PotentialBrandtFull(currentCoord, pFullEnergy.Z, 1, pFullEnergy.lambda);
		if (((tmpResult-result>0)&(sign<0)) || ((tmpResult-result<0)&(sign>0)))
//		{
//			printf("extremal point in:\t%.03f\n", currentCoord);
//			printf("energy in extremal point is:\t%.03f\n", -tmpResult);
//			sign *= -1;
//		}
//		result = tmpResult;
//		printf("%.03f\n", currentCoord);
		fprintf(fd, "%.03f\n", -result);

		tmpResult = PotentialBrandtFull(currentCoord, pFullEnergy.Z, 0, 0.308);
		if (((tmpResult-result>0)&(sign<0)) || ((tmpResult-result<0)&(sign>0)))
		{
			printf("extremal point in:\t%.03f\n", currentCoord);
			printf("energy in extremal point is:\t%.03f\n", -tmpResult);
			sign *= -1;
		}
		result = tmpResult;
//		printf("%.03f\n", currentCoord);
		fprintf(fx, "%.05f\n", -tmpResult);

	}
	fclose(fd);
	fclose(fx);
}

double GetEffectiveCharge (char* filename, int numberOfSteps, int l)
{
	double qEff = 0;
	double step = 5/(double)numberOfSteps;
	double result = -100;
	double tmpResult;
	pFullEnergy.q = 0.1;
	pFullEnergy.l = l;
	// FILE* fq = fopen("res_charge.txt", "w");
	FILE* fe = fopen(filename, "w");
	for (int i = 0; i<numberOfSteps; i++)
	{
		pFullEnergy.q += step;
		// printf("Effective charge is:\t%.03f", pFullEnergy.q);
		tmpResult = ResidualIntegral(&pFullEnergy) - pFullEnergy.q*pFullEnergy.q/4;
		if(tmpResult > result) {
			qEff = pFullEnergy.q;
			result = tmpResult;
		}
		fprintf(fe, "%.05f, %.05f\n", pFullEnergy.q, tmpResult);
		// fprintf(fq, "%.05f\n", pFullEnergy.q);
		// printf("tmpResult is:\t%.02f\n", tmpResult);
	}
	printf("Effective charge is:\t%.03f\n", qEff);
	fclose(fe);
	// fclose(fq);
	return qEff;
}




int main(void) 
{
	int numberOfSteps = 200;

	// printf("Effective charge from BK distribution is; %.05f\n", GetQEffectiveBrandt(&pFullEnergy));
	// GetBKPotetialfromPrinc("BK.csv", &pFullEnergy, numberOfSteps);
	pFullEnergy.l = 0;
	double qEffl0 = GetEffectiveCharge("res_energy_0.txt",numberOfSteps, 0);
	pFullEnergy.q = qEffl0;
	// pFullEnergy.q = 3.22;
	GetInhomogeneousDistributionWitnEff("lambda_1.txt", lambda, numberOfSteps, pFullEnergy.q, 0);
	printf("Full energy for 2s is: %.05f\n", FullEnergyIntegral(&pFullEnergy)*energyConv);
	pFullEnergy.l = 1;
	double qEffl1 = GetEffectiveCharge("res_energy_1.txt",numberOfSteps, 1);
	// pFullEnergy.q = 3.14;
	pFullEnergy.q = qEffl1;
	printf("Full energy for 2p is: %.05f\n", FullEnergyIntegral(&pFullEnergy)*energyConv);
	GetInhomogeneousDistributionWitnEff("lambda_2.txt", lambda, numberOfSteps, pFullEnergy.q, 1);
	pFullEnergy.q = 5.6;
	pFullEnergy.n = 1;
	double qEff1s = GetEffectiveCharge("res_energy_1s.txt",numberOfSteps, 0);
	pFullEnergy.q = qEff1s;
	GetInhomogeneousDistributionWitnEff("lambda_3.txt", lambda, numberOfSteps, pFullEnergy.q, 0);
	printf("Full energy for 1s is: %.05f\n", FullEnergyIntegral(&pFullEnergy)*energyConv);
	// GetXAxis(numberOfSteps);
	// GetInhomogeneousDistribution("lambda_0.6.txt", lambda*0.6, numberOfSteps);
	// GetInhomogeneousDistribution("lambda_0.8.txt", lambda*0.8, numberOfSteps);
	
	// GetTwoFieldsDistribution("two_ions.csv", lambda, numberOfSteps);
	// GetInhomogeneousDistribution("lambda_1.2.txt", lambda*1.2, numberOfSteps);
	// GetInhomogeneousDistribution("lambda_1.4.txt", lambda*1.4, numberOfSteps);
	// GetHomogeneousDistribution("MainBrandt.txt", lambda, numberOfSteps);
	// (void) GetEffectiveCharge("res_energy_0.txt",numberOfSteps, 0);
	// (void) GetEffectiveCharge("res_energy_1.txt", numberOfSteps, 1);
//	GetExtremalPoints();
//	printf("%.02f\n", IntegralOverW (2,1,0,0));
////	printf("%.02f\n", IntegralOverW (2,2,0,0));
//	printf("%.02f\n", IntegralOverW (2,0,0,1));
////	printf("%.02f\n", IntegralOverW (2,1,0,1));
//	printf("%.02f\n", FullEnergyIntegral (&pFullEnergy));





    return 0;
}

