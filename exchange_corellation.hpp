
#pragma once

#include <iostream>
#include <math.h>
#include <complex>
#include <fstream>
#include <string>
#include <vector>
#include <numeric>
#include <map>
#include <algorithm>
#include "basis.hpp"
#include <chrono>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_math.h>


typedef struct _gsl_params_XC {
    void* instance;
    // Basis_function basis_i;
    // Basis_function basis_j;
    int i;
    int j;
    float x;
    float y;
    float z;
} gsl_params_XC;

class ExchangeCorrelation {
    public:
     vector<Basis_function> basis_set_;
     vector<vector<float> > density_matrix_;
    ExchangeCorrelation(vector<Basis_function> basis_set, vector<vector<float> > density_matrix);
    static float Calculate_Slater_Exchange(float density);
    static float wigner_seitz_radius(float density);
    static float VoskoWilkNusair_potential(float density);
    float electron_density(float x, float y, float z);
    // static float integrand(ExchangeCorrelation* instance, int i, int j, float rho, float theta, float phi);
    double get_integral(int i, int j);
    double get_dummy_integral(int i, int j);
    double integrand(int i, int j, float rho, float theta, float phi);
    // static gsl_params_XC gsl_params_Z;
    // static gsl_params_XC gsl_params_Y;
    // static gsl_params_XC gsl_params_X;
    // static double _gsl_wrap_Z (double x, void* params);
    // static double _gsl_wrap_Y (double x, void* params);
    // static double _gsl_wrap_X (double x, void* params);
    // static const gsl_function gsl_wrap_Z;
    // static const gsl_function gsl_wrap_Y;
    // static const gsl_function gsl_wrap_X;


};
    static gsl_params_XC gsl_params_Z;
    static gsl_params_XC gsl_params_Y;
    static gsl_params_XC gsl_params_X;
    static double _gsl_wrap_Z (double x, void* params);
    static double _gsl_wrap_Y (double x, void* params);
    static double _gsl_wrap_X (double x, void* params);
    static double integrand(ExchangeCorrelation* instance, int i, int j, float rho, float theta, float phi);

// typedef struct _gsl_params_XC {
//     ExchangeCorrelation* instance;
//     // Basis_function basis_i;
//     // Basis_function basis_j;
//     int i;
//     int j;
//     float x;
//     float y;
//     float z;
// } gsl_params_XC;
