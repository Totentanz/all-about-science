import matplotlib.pyplot as plt
import sys, os, math as m
import numpy as np 
import scipy as sp
from scipy.integrate import quad, dblquad
import scipy.special as spsp
# import brandt

s = 0.7
a = -1.2
b = 1.4
R = 50
step = 100

def j1(l, z):
    return spsp.jv(l + 0.5, z)

def j2(l, z):
    return spsp.jv(-(l + 0.5), z)

def j1_der(l, z):
    return spsp.jvp(l + 0.5, z)

def j2_der(l, z):
    return spsp.jvp(-(l + 0.5), z)

# def Nl(n, l, q):
#     return quad(lambda x:(brandt.RadialHWf(x, n, l, q)*brandt.RadialHWf(x, n, l, q)*x*x, 0, s)


def get_first_norm(l, kappa):
    first_bracket = (j1_der(l, kappa*(R+a)) - j2_der(l, kappa*(R+a)*(j1(l, kappa*(R+a))/j2(l, kappa*(R+a)))))*a
    second_bracket = (j1_der(l, kappa*(R+b)) - j2_der(l, kappa*(R+b)*(j1(l, kappa*(R+b))/j2(l, kappa*(R+b))))*b)
    return np.sqrt((pow(first_bracket, 2)/2 - pow(second_bracket, 2)/2))

def get_norm(l, kappa):
    first_norm = get_first_norm(l, kappa)
    second_norm = - first_norm*(j1(l, kappa*(R+a))/j2(l, kappa*(R+a)))
    print(first_norm, second_norm)
    return first_norm, second_norm


def find_kappa(l):
    old_res = 0.1
    near_zero_kappa = []
    for current_kappa in range(0, 1000):
        current_kappa /= step 
        if current_kappa != 0:
            res = j1(l, current_kappa*(R+a))*j2(l, current_kappa*(R+b)) - j1(l, current_kappa*(R+b))*j2(l, current_kappa*(R+a))
            sign_res = res/m.fabs(res)
            sign_old_res = old_res/m.fabs(old_res)
            # print(old_res, res)
            if( sign_old_res != sign_res):
                near_zero_kappa.append(current_kappa)
                print("for l", l, "kappa is", current_kappa)
                print(old_res, res)
            old_res = res

def WaveFunctionII(r, phi, teta, P, M, N, kappa):
    C1, C2 = get_norm(N, kappa)
    return np.sqrt((m.pi*kappa)/2*r)*(C1*j1(N, kappa*r) + C2*j2(N, kappa*r))*spsp.sph_harm(M, P, phi, teta)



# for l in range(5):
#     find_kappa(l)

# get_norm(4, 1.22)

# current_kappa = 1
# l = 0
# print(j1(l, current_kappa*(R+a)), j2(l, current_kappa*(R+b)), j1(l, current_kappa*(R+b)), j2(l, current_kappa*(R+a)))