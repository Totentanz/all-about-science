all: 
	gcc src/*.c -o main -lgsl -lgslcblas -lm -I/.

DFT:
	g++-8 KS.cpp basis.cpp energy.cpp head-gordon-pople.cpp exchange_corellation.cpp -o KS -g -ltbb -lgsl -lgslcblas -fno-omit-frame-pointer