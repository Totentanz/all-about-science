#pragma once

#include <eigen3/Eigen/Dense>
#include "basis.hpp"

// #include "GaussianIntegral.h"

// #include "TensorOrder3.h"
// #include "TensorOrder4.h"

namespace GaussianIntegrals {


	// the main source of information for implementing this is:
	// HSERILib: Gaussian integral evaluation
	// link: http://theory.rutgers.edu/~giese/notes/HSERILib.pdf
	// it discusses electron-electron integrals but also other kind of integrals very shortly

	// class IntegralsRepository;

	class GaussianTwoElectrons : public GaussianIntegral
	{
	public:
		Eigen::MatrixXd matrixCalc;
		Tensors::TensorOrder3<double> tensor3Calc;
		Tensors::TensorOrder4<double> tensor4Calc;

		GaussianTwoElectrons();
		~GaussianTwoElectrons();

		double getValue(const vector<float>& QN1, const vector<float>& QN2, const vector<float>& QN3, const vector<float>& QN4);

		void Reset(IntegralsRepository* repository, double alpha1, double alpha2, double alpha3, double alpha4, 
			const Vector3D<double>& center1, const Vector3D<double>& center2, const Vector3D<double>& center3, const Vector3D<double>& center4, 			
			unsigned int maxL1, unsigned int maxL2, unsigned int maxL3, unsigned int maxL4);

	protected:
		void VerticalRecursion(double alpha, double alpha12, const Vector3D<double>& Rpa, const Vector3D<double>& Rwp, unsigned int maxL);
		void ElectronTransfer(double alpha12, double alpha34, const Vector3D<double>& delta, unsigned int maxL, unsigned int maxL2);

		inline static bool DecrementPrevAndPrevPrevAndSetN(unsigned int& prev, unsigned int& prevPrev, double& N)
		{
			--prev;
			if (prev > 0) {
				N = prev;
				prevPrev -= 2;

				return true;
			}
			
			return false;			
		}

		inline static bool IncNextDecPrevSetN(unsigned int& next, unsigned int& prev, double& N)
		{
			++next;
			if (prev > 0) {
				N = prev;
				--prev;
				
				return true;
			}

			return false;
		}

		inline static bool GetPrevAndPrevPrevAndScalarForElectronTransfer(const vector<float>& currentQN, const Vector3D<double>& delta, vector<float> &prevQN, vector<float> &prevPrevQN, double &deltaScalar, double& N, unsigned int& maxIndex)
		{
			prevPrevQN = prevQN = currentQN;

			maxIndex = currentQN.MaxComponentVal();

			N = 0;

			bool addPrevPrev;

			if (currentQN[0]== maxIndex)
			{
				addPrevPrev = DecrementPrevAndPrevPrevAndSetN(prevQN[0], prevPrevQN[0], N);

				deltaScalar = delta[0];
			}
			else if (currentQN[0] == maxIndex)
			{
				addPrevPrev = DecrementPrevAndPrevPrevAndSetN(prevQN[1], prevPrevQN[1], N);

				deltaScalar = delta[1];
			}
			else
			{
				addPrevPrev = DecrementPrevAndPrevPrevAndSetN(prevQN[2], prevPrevQN[2], N);

				deltaScalar = delta[2];
			}

			return addPrevPrev;
		}
	public:
		void HorizontalRecursion1(const Vector3D<double>& dif, unsigned int L1, unsigned int L2, unsigned int L3, unsigned int L4);

		void HorizontalRecursion2(const Vector3D<double>& dif, unsigned int L1, unsigned int L2, unsigned int L3, unsigned int L4);

		
		inline static bool GetPrevAndPrevPrevAndScalarsForVerticalRecursion(const vector<float>& currentQN, const vector<float>& Rpa, \
																			const vector<float>& Rwp, const vector<float>& prevQN, \
																			vector<float>& prevPrevQN, double& RpaScalar, double& RwpScalar, \
																			double& N)
		{
			prevPrevQN = prevQN = currentQN;

			const unsigned int maxIndex = currentQN.MaxComponentVal();

			N = 0;

			bool addPrevPrev;

			if (currentQN[0]== maxIndex)
			{
				addPrevPrev = DecrementPrevAndPrevPrevAndSetN(prevQN[0], prevPrevQN[0], N);

				RpaScalar = Rpa[0];
				RwpScalar = Rwp[0];
			}
			else if (currentQN[0] == maxIndex)
			{
				addPrevPrev = DecrementPrevAndPrevPrevAndSetN(prevQN[1], prevPrevQN[1], N);

				RpaScalar = Rpa[1];
				RwpScalar = Rwp[1];
			}
			else
			{
				addPrevPrev = DecrementPrevAndPrevPrevAndSetN(prevQN[2], prevPrevQN[2], N);

				RpaScalar = Rpa[2];
				RwpScalar = Rwp[2];
			}

			return addPrevPrev;
		}

		inline static double GetNextAndPrevQNAndScalarDiffForHorizontalRecursion(const vector<float>& integral_exponents1, const vector<float>& integral_exponents2, \
																				 const vector<float>& dif, vector<float>& next_integral_exponents1, \
																				 vector<float>& prev_integral_exponents2)
		{
			next_integral_exponents1 = integral_exponents1;
			prev_integral_exponents2 = integral_exponents2;

			const unsigned int maxIndex = max(integral_exponents2);

			double difScalar;

			if (integral_exponents2[0] == maxIndex)
			{
				++next_integral_exponents1[0];
				--prev_integral_exponents2[0];

				difScalar = dif[0];
			}
			else if (integral_exponents2[1] == maxIndex)
			{
				++next_integral_exponents1[1];
				--prev_integral_exponents2[1];

				difScalar = dif[1];
			}
			else
			{
				++next_integral_exponents1[2];
				--prev_integral_exponents2[2];

				difScalar = dif[2];
			}

			return difScalar;
		}

		inline static void IncrementQNandDecrementLimitIfNeeded(vector<float>& integral_exponents, unsigned int& limit)
		{
			const unsigned int oldL = int(integral_exponents[0] + integral_exponents[1] + integral_exponents[2]);
			int sum = oldL;

			++sum;
			if (sum != oldL) {
				assert(limit > 0);
				--limit;
			}
		}
	};

}
