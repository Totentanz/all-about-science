#include <iostream>
#include <math.h>
#include <complex>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <string>
#include <cstring>
#include "energy.hpp"
#include "exchange_corellation.hpp"

float effective_charge = 3.14;
float alpha = 0.2655;

using namespace std;

float Coulomb_law (float charge_1, float charge_2, float distance) {
    return charge_2 /distance;
}

float Coulomb_law_screened (float charge_1, float charge_2, float distance) {
    // return (charge_2 - (charge_2 - 1 ) * exp(-distance/(alpha*0,53)))/distance;
    return (charge_2 - (charge_2 - 1 ) * exp(-distance/(alpha*0.53)))/distance + (charge_2-1)* exp(-distance/(alpha*0,53))/(2.0*alpha*0.53);

}

void Energy::Set_Coulomb_matrix(vector<map<string, vector<float> > > nuclei_list) {
    //     for i in range(matrix_length):
    coulomb_law_matrix_eigen_.resize(nuclei_list.size(), nuclei_list.size());
    for (int i = 0; i< nuclei_list.size(); i++) {
        vector<float> coord_1 = nuclei_list[i]["nuclei_coordinates"];
        // coulomb_law_matrix_.push_back({0});
        for (int j = 0; j< nuclei_list.size(); j++) {
            vector<float> coord_2 = nuclei_list[j]["nuclei_coordinates"];
            if (i != j) {
                // vector<float> coord_1 = nuclei_list[i]
                float distance = coordinate_distance(coord_1, coord_2);
                // float distance = sqrt(  (coord_1[0] - coord_2[0])*(coord_1[0] - coord_2[0]) + (coord_1[1] - coord_2[1])*(coord_1[1] - coord_2[1]) + (coord_1[2] - coord_2[2])*(coord_1[2] - coord_2[2]) );
                
                if (coulomb_law_matrix_.size() < i+1) {
                    coulomb_law_matrix_.push_back({nuclei_list[i]["charge"][0] * nuclei_list[j]["charge"][0] /distance});
                } else {
                    coulomb_law_matrix_[i].push_back(nuclei_list[i]["charge"][0] * nuclei_list[j]["charge"][0] /distance);
                    }
                    // cout << coulomb_law_matrix_[i][j] << endl;
            } else {
                if (coulomb_law_matrix_.size() < i+1) {
                    coulomb_law_matrix_.push_back({0}); // first element (0,0)
                } else {
                    coulomb_law_matrix_[i].push_back(0);
                }
            }
            coulomb_law_matrix_eigen_(i,j) = coulomb_law_matrix_[i][j];
        }
    }
};

void Energy::Set_Coulomb_matrix_screened(vector<map<string, vector<float> > > nuclei_list) {
    //     for i in range(matrix_length):
    coulomb_law_matrix_eigen_.resize(nuclei_list.size(), nuclei_list.size());
    for (int i = 0; i< nuclei_list.size(); i++) {
        vector<float> coord_1 = nuclei_list[i]["nuclei_coordinates"];
        // coulomb_law_matrix_.push_back({0});
        for (int j = 0; j< nuclei_list.size(); j++) {
            vector<float> coord_2 = nuclei_list[j]["nuclei_coordinates"];
            if (i != j) {
                // vector<float> coord_1 = nuclei_list[i]
                float distance = coordinate_distance(coord_1, coord_2);
                // float distance = sqrt(  (coord_1[0] - coord_2[0])*(coord_1[0] - coord_2[0]) + (coord_1[1] - coord_2[1])*(coord_1[1] - coord_2[1]) + (coord_1[2] - coord_2[2])*(coord_1[2] - coord_2[2]) );
                
                if (coulomb_law_matrix_.size() < i+1) {
                    coulomb_law_matrix_.push_back({Coulomb_law_screened(nuclei_list[i]["charge"][0], nuclei_list[j]["charge"][0], distance)});
                } else {
                    coulomb_law_matrix_[i].push_back(Coulomb_law_screened(nuclei_list[i]["charge"][0], nuclei_list[j]["charge"][0], distance));
                    }
                    // cout << coulomb_law_matrix_[i][j] << endl;
            } else {
                if (coulomb_law_matrix_.size() < i+1) {
                    coulomb_law_matrix_.push_back({effective_charge}); // first element (0,0)
                } else {
                    coulomb_law_matrix_[i].push_back(effective_charge);
                }
            }
            coulomb_law_matrix_eigen_(i,j) = coulomb_law_matrix_[i][j];
        }
    }
};

void Energy::Set_orbital_overlap_matrix(vector<Basis_function> basis_set) {
    orbital_overlap_matrix_.resize(basis_set.size());
    for (int row = 0; row < orbital_overlap_matrix_.size(); row++) orbital_overlap_matrix_[row].resize(basis_set.size());
    orbital_overlap_matrix_eigen_.resize(basis_set.size(), basis_set.size());

    for (int i = 0; i< basis_set.size(); i++) {
        for (int j = 0; j< basis_set.size(); j++) {
            float matrix_element = 0.0;
            // here we get overlap for all primitives
            for (int a = 0; a < basis_set[i].exponent_.size(); a++) {
                for (int b = 0; b < basis_set[j].exponent_.size(); b++) {
                    // cout << "i'm here!\t" << orbital_overlap(basis_set[i].exponent_[a], basis_set[i].coordinates_, basis_set[i].integral_exponents_, \
                    //                     basis_set[j].exponent_[b], basis_set[j].coordinates_, basis_set[j].integral_exponents_) << endl;
                    matrix_element += basis_set[i].gaussian_norms_[a] * basis_set[j].gaussian_norms_[b] * \
                        basis_set[i].contraction_[a] * basis_set[j].contraction_[b] * \
                        orbital_overlap(basis_set[i].exponent_[a], basis_set[i].coordinates_, basis_set[i].integral_exponents_, \
                                        basis_set[j].exponent_[b], basis_set[j].coordinates_, basis_set[j].integral_exponents_);

                }
            }
            orbital_overlap_matrix_[i][j] = matrix_element;
            orbital_overlap_matrix_eigen_(i,j) = matrix_element;
            // cout << "matrix_element \t" << matrix_element << endl;
        }
    }
}

void Energy::Set_kinetic_energy_matrix(vector<Basis_function> basis_set) {
    kinetic_energy_matrix_.resize(basis_set.size());
    for (int row = 0; row < kinetic_energy_matrix_.size(); row++) kinetic_energy_matrix_[row].resize(basis_set.size());
    kinetic_energy_matrix_eigen_.resize(basis_set.size(), basis_set.size());

    for (int i = 0; i< basis_set.size(); i++) {
        for (int j = 0; j< basis_set.size(); j++) {
            float matrix_element = 0.0;
            
            // here we get overlap for all primitives
            for (int a = 0; a < basis_set[i].exponent_.size(); a++) {
                for (int b = 0; b < basis_set[j].exponent_.size(); b++) {
                    vector<Basis_function> del_primitives = del_operator(basis_set[j].exponent_[b], basis_set[j].coordinates_, basis_set[j].integral_exponents_, basis_set[j].gaussian_norms_[b]);
                    float priimitives_element = 0;
                    // cout << "del_primitives\t" << del_primitives.size() << endl;
                    for (int primitive_number = 0; primitive_number < del_primitives.size(); primitive_number++) {
                        priimitives_element += basis_set[i].gaussian_norms_[a] * basis_set[j].gaussian_norms_[j] *\
                        basis_set[i].exponent_[a] * basis_set[i].exponent_[b] *\
                        del_primitives[primitive_number].contraction_[0] * \
                        orbital_overlap(basis_set[i].exponent_[a], basis_set[i].coordinates_, basis_set[i].integral_exponents_, \
                                        del_primitives[primitive_number].exponent_[0], del_primitives[primitive_number].coordinates_, del_primitives[primitive_number].integral_exponents_);
                    }
                    matrix_element += priimitives_element;
                }
            }
            kinetic_energy_matrix_[i][j] = basis_set[i].normalisation_*basis_set[j].normalisation_*matrix_element;
            kinetic_energy_matrix_eigen_(i,j) = kinetic_energy_matrix_[i][j];
            // cout << "kinetic_matrix_element \t" << kinetic_energy_matrix_[i][j] << endl;
        }
    }
}

void Energy::Set_nuclear_attraction_matrix(vector<Basis_function> basis_set, vector<map<string, vector<float> > > nuclei_list) {
    nuclear_attraction_matrix_.resize(basis_set.size());
    for (int row = 0; row < nuclear_attraction_matrix_.size(); row++) nuclear_attraction_matrix_[row].resize(basis_set.size());
    nuclear_attraction_matrix_eigen_.resize(basis_set.size(), basis_set.size());

    for (int i = 0; i< basis_set.size(); i++) {
        for (int j = 0; j< basis_set.size(); j++) {
            float matrix_element = 0.0;
            
            // here we get overlap for all primitives
            for (int a = 0; a < basis_set[i].exponent_.size(); a++) {
                for (int b = 0; b < basis_set[j].exponent_.size(); b++) {
                    for (int nuclei_number = 0; nuclei_number < nuclei_list.size(); nuclei_number++) {
                        // matrix_element += nuclei_list[nuclei_number]["charge"][0] * basis_set[i].exponent_[a] * basis_set[i].contraction_[a] 
                        matrix_element += 6 * basis_set[i].exponent_[a] * basis_set[i].contraction_[a] \
                                          * basis_set[j].exponent_[b] * basis_set[j].contraction_[b] \
                                          * nuclear_attraction(basis_set[i].exponent_[a], basis_set[i].coordinates_, basis_set[i].integral_exponents_, \
                                                               basis_set[j].exponent_[b], basis_set[j].coordinates_, basis_set[j].integral_exponents_, \
                                                               nuclei_list[nuclei_number]["nuclei_coordinates"]);
                    }
                }
            }
            nuclear_attraction_matrix_[i][j] = matrix_element*basis_set[i].normalisation_*basis_set[j].normalisation_;
            nuclear_attraction_matrix_eigen_(i,j) = nuclear_attraction_matrix_[i][j];
            // cout << "nuclear_attraction_matrix_[i][j]\t" << nuclear_attraction_matrix_[i][j] << endl;
        }
    }
}

void Energy::Set_core_hamiltonian_matrix() {
    core_hamiltonian_matrix_eigen_.resize(kinetic_energy_matrix_.size(), kinetic_energy_matrix_.size());
    // core_hamiltonian_matrix_eigen_ = kinetic_energy_matrix_eigen_ + nuclear_attraction_matrix_eigen_ + coulomb_law_matrix_eigen_;
    cout << kinetic_energy_matrix_eigen_.size() << endl << coulomb_law_matrix_eigen_.size() << endl;
    core_hamiltonian_matrix_eigen_ = kinetic_energy_matrix_eigen_;


    core_hamiltonian_matrix_.resize(kinetic_energy_matrix_.size());
    for (int row = 0; row < kinetic_energy_matrix_.size(); row++) core_hamiltonian_matrix_[row].resize(kinetic_energy_matrix_.size());


    for (int i = 0; i< kinetic_energy_matrix_.size(); i++) {
        for (int j = 0; j< kinetic_energy_matrix_.size(); j++) {
            // core_hamiltonian_matrix_[i][j] = kinetic_energy_matrix_[i][j] + nuclear_attraction_matrix_[i][j] + coulomb_law_matrix_[i][j];
            core_hamiltonian_matrix_[i][j] = kinetic_energy_matrix_[i][j]; 
            // core_hamiltonian_matrix_eigen_ = kinetic_energy_matrix_eigen_;


        }
    }
    for (int k = 0; k < coulomb_law_matrix_.size();k++) {
        for (int n = 0; n < coulomb_law_matrix_.size(); n++) {
            
            // core_hamiltonian_matrix_[5*k][5*n] += coulomb_law_matrix_[k][n]; 
            // core_hamiltonian_matrix_[5*k+1][5*n+1] += coulomb_law_matrix_[k][n]; 
            // core_hamiltonian_matrix_[5*k+2][5*n+2] += coulomb_law_matrix_[k][n]; 
            // core_hamiltonian_matrix_[5*k+3][5*n+3] += coulomb_law_matrix_[k][n]; 
            // core_hamiltonian_matrix_[5*k+4][5*n+4] += coulomb_law_matrix_[k][n];

            // core_hamiltonian_matrix_eigen_(5*k,5*n) += coulomb_law_matrix_eigen_( k,n);
            // core_hamiltonian_matrix_eigen_(5*k+1,5*n+1) += coulomb_law_matrix_eigen_(k,n);
            // core_hamiltonian_matrix_eigen_(5*k+2,5*n+2) += coulomb_law_matrix_eigen_(k,n);
            // core_hamiltonian_matrix_eigen_(5*k+3,5*n+3) += coulomb_law_matrix_eigen_(k,n);
            // core_hamiltonian_matrix_eigen_(5*k+4,5*n+4) += coulomb_law_matrix_eigen_(k,n);
            for (int x = 0; x< 10; x ++) {
                for (int y = 0; y<10; y++) {
                    core_hamiltonian_matrix_[x*k+y][x*n+y] += coulomb_law_matrix_[k][n];
                }
            }
            // core_hamiltonian_matrix_[5*k][5*n] += coulomb_law_matrix_[k][n]; 
            // core_hamiltonian_matrix_[5*k+1][5*n+1] += coulomb_law_matrix_[k][n]; 
            // core_hamiltonian_matrix_[5*k+2][5*n+2] += coulomb_law_matrix_[k][n]; 
            // core_hamiltonian_matrix_[5*k+3][5*n+3] += coulomb_law_matrix_[k][n]; 
            // core_hamiltonian_matrix_[5*k+4][5*n+4] += coulomb_law_matrix_[k][n];

            // core_hamiltonian_matrix_eigen_(5*k,5*n) += coulomb_law_matrix_eigen_( k,n);
            // core_hamiltonian_matrix_eigen_(5*k+1,5*n+1) += coulomb_law_matrix_eigen_(k,n);
            // core_hamiltonian_matrix_eigen_(5*k+2,5*n+2) += coulomb_law_matrix_eigen_(k,n);
            // core_hamiltonian_matrix_eigen_(5*k+3,5*n+3) += coulomb_law_matrix_eigen_(k,n);
            // core_hamiltonian_matrix_eigen_(5*k+4,5*n+4) += coulomb_law_matrix_eigen_(k,n);
        }
    }
}

void Energy::Set_repulsion_tensor(vector<Basis_function> basis_set) {
    cout << "begin to calculate repulsion tensor" << endl;
    repulsion_tensor_.resize(basis_set.size());
    for (int a = 0; a < basis_set.size(); a++) {
        repulsion_tensor_[a].resize(basis_set.size());
        for (int b = 0; b < basis_set.size(); b++) {
            repulsion_tensor_[a][b].resize(basis_set.size());
            for (int c = 0; c < basis_set.size(); c++) {
                repulsion_tensor_[a][b][c].resize(basis_set.size());
            }
        } 
    }
    tbb::task_group g;
    tbb::task_arena arena(tbb::task_arena::automatic);
    // tbb::task_arena arena(1);

    auto start = std::chrono::system_clock::now();
    for (int a = 0; a < basis_set.size(); a++) {
        for (int b = 0; b < basis_set.size(); b++) {
            for (int c = 0; c < basis_set.size(); c++) {
                for (int d = 0; d < basis_set.size(); d++) {
                    if (!(a > b | c > d | a > c | (a == c & b > d))) {
                        float distance_ij = coordinate_distance(basis_set[a].coordinates_, basis_set[b].coordinates_);
                        float distance_ik = coordinate_distance(basis_set[a].coordinates_, basis_set[c].coordinates_);
                        float distance_il = coordinate_distance(basis_set[a].coordinates_, basis_set[d].coordinates_);
                        float distance_jk = coordinate_distance(basis_set[b].coordinates_, basis_set[c].coordinates_);
                        float distance_jl = coordinate_distance(basis_set[b].coordinates_, basis_set[d].coordinates_);
                        float distance_kl = coordinate_distance(basis_set[c].coordinates_, basis_set[d].coordinates_);
                        float sum_distance = distance_kl + distance_jl + distance_jk + distance_ik + distance_il + distance_ij;

                        if ((sum_distance > 2.0)  ) {
                            // cout << "start to count exchange" << endl;
                                repulsion_tensor_[a][b][c][d] = 0.0;
                        } else {
                            if ((basis_set[a].marker_ > 3 ) | (basis_set[b].marker_ > 3) | ( basis_set[c].marker_ > 3) | (basis_set[d].marker_ > 3)) {
                                // cout << "set basises " << a << " "<< b << " " << c << " " << d << "to integrate" << endl;;
                                arena.execute([&]{
                                    g.run([&, a, b, c, d]() {
                                        cout << "value " << a << " "<< b << " " << c << " " << d << " ";
                                        HGP_Get_Value value = HGP_Get_Value();
                                
                                        repulsion_tensor_[a][b][c][d] = value.hgp_integrate(basis_set[a], basis_set[b], basis_set[c], basis_set[d]);
                                cout << " is set with " << repulsion_tensor_[a][b][c][d] << endl;
                                    });
                                });
                            }
                        }
                    }
                }
            }
        } 
    }

    arena.execute([&] {g.wait();});

    auto end_element = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed = end_element - start;
    std::cout << "Elapsed time for element: " << elapsed.count() << "s" << endl;

    ofstream repulsion_tensor;
    repulsion_tensor.open("repulsion_tensor_6-31g.dat");
    for (int a = 0; a < basis_set.size(); a++) {
        for (int b = 0; b < basis_set.size(); b++) {
            for (int c = 0; c < basis_set.size(); c++) {
                for (int d = 0; d < basis_set.size(); d++) {
                    if (!(a > b | c > d | a > c | (a == c & b > d))) {
                        repulsion_tensor << a << " "<< b << " " << c << " " << d << " " << repulsion_tensor_[a][b][c][d] << endl;

                        vector<int> combinations = {a,b,c,d};
                        sort(combinations.begin(), combinations.end());
                        while(next_permutation(combinations.begin(), combinations.end())) {
                            repulsion_tensor_[combinations[0]][combinations[1]][combinations[2]][combinations[3]] = repulsion_tensor_[a][b][c][d];
                        }
                    }
                }
            }
        } 
    }
    repulsion_tensor.close();
}

void Energy::mock_Set_repulsion_tensor(vector<Basis_function> basis_set) {
    repulsion_tensor_.resize(basis_set.size());
    for (int a = 0; a < basis_set.size(); a++) {
        repulsion_tensor_[a].resize(basis_set.size());
        for (int b = 0; b < basis_set.size(); b++) {
            repulsion_tensor_[a][b].resize(basis_set.size());
            for (int c = 0; c < basis_set.size(); c++) {
                repulsion_tensor_[a][b][c].resize(basis_set.size());
            }
        } 
    }
    for (int a = 0; a < basis_set.size(); a++) {
        for (int b = 0; b < basis_set.size(); b++) {
            for (int c = 0; c < basis_set.size(); c++) {
                for (int d = 0; d < basis_set.size(); d++) {
                    repulsion_tensor_[a][b][c][d] = 0.0;
                }
            }
        } 
    }
}

void Energy::Set_repulsion_tensor_from_file(vector<Basis_function> basis_set) {
    repulsion_tensor_.resize(basis_set.size());
    for (int a = 0; a < basis_set.size(); a++) {
        repulsion_tensor_[a].resize(basis_set.size());
        for (int b = 0; b < basis_set.size(); b++) {
            repulsion_tensor_[a][b].resize(basis_set.size());
            for (int c = 0; c < basis_set.size(); c++) {
                repulsion_tensor_[a][b][c].resize(basis_set.size());
            }
        } 
    }
    ifstream repulsion_tensor;
    repulsion_tensor.open("repulsion_tensor.dat");
    string one_line;
    while(getline(repulsion_tensor, one_line)) {
        char *cstr = new char[one_line.length() + 1];
        strcpy(cstr, one_line.c_str());
        int a;
        int b;
        int c;
        int d;
        float value;
        char* pch = strtok (cstr, " ");
        // while(pch) {
            a = atoi(pch);
            pch = strtok (NULL, " ");

            b = atoi(pch);
            pch = strtok (NULL, " ");

            c = atoi(pch);
            pch = strtok (NULL, " ");

            d = atoi(pch);
            pch = strtok (NULL, " ");

            value = atof(pch);
            repulsion_tensor_[a][b][c][d] = value;
            // pch = strtok (NULL, " ");
            // cout << "d is " << d << "value is " << value << endl;
        // }
    }
    repulsion_tensor.close();
    for (int a = 0; a < basis_set.size(); a++) {
        for (int b = 0; b < basis_set.size(); b++) {
            for (int c = 0; c < basis_set.size(); c++) {
                for (int d = 0; d < basis_set.size(); d++) {
                    repulsion_tensor_[a][b][c][d] = 0.0;
                    if (!(a > b | c > d | a > c | (a == c & b > d))) {
                        vector<int> combinations = {a,b,c,d};
                        sort(combinations.begin(), combinations.end());
                        while(next_permutation(combinations.begin(), combinations.end())) {
                            repulsion_tensor_[combinations[0]][combinations[1]][combinations[2]][combinations[3]] = repulsion_tensor_[a][b][c][d];
                        }
                    }
                }
            }
        } 
    }
}

void Energy::Set_exchange_corellation_matrix(vector<Basis_function> basis_set, vector<vector<float> > density_matrix) {
    exchange_corellation_matrix_eigen_.resize(basis_set.size(),basis_set.size());
    exchange_corellation_matrix_.resize(basis_set.size());
    for (int row = 0; row < basis_set.size(); row++) exchange_corellation_matrix_[row].resize(basis_set.size());

    ExchangeCorrelation XC_object = ExchangeCorrelation(basis_set, density_matrix);
    tbb::task_group g;
    tbb::task_arena arena(tbb::task_arena::automatic);
    // tbb::task_arena arena(1);

    auto start = std::chrono::system_clock::now();
    for (int i = 0; i < basis_set.size(); i++) {
        for (int j = 0; j < basis_set.size(); j++) {
            if (j >= i) {
                arena.execute([&]{
                            g.run([&, i, j]() {
                                exchange_corellation_matrix_[i][j] = XC_object.get_dummy_integral(i,j); //TODO
                                // exchange_corellation_matrix_[i][j] = XC_object.get_integral(i,j); //TODO

                            });
                        });
                // // exchange_corellation_matrix_[i][j] = XC_object.get_integral(i,j); //TODO
                // exchange_corellation_matrix_[j][i] = exchange_corellation_matrix_[i][j];
                // cout << "element value is:\t" << exchange_corellation_matrix_[i][j] << endl;
                // exchange_corellation_matrix_eigen_(j,i) = exchange_corellation_matrix_[i][j];
            }
        }
    }

    arena.execute([&] {g.wait();});

    auto end_element = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed = end_element - start;
    std::cout << "Elapsed time for exchange_row: " << elapsed.count() << "s" << endl;

    for (int i = 0; i < basis_set.size(); i++) {
        for (int j = 0; j < basis_set.size(); j++) {
            if (j >= i) {
                // exchange_corellation_matrix_[i][j] = XC_object.get_integral(i,j); //TODO
                exchange_corellation_matrix_[j][i] = exchange_corellation_matrix_[i][j];
                cout << "element value is:\t" << exchange_corellation_matrix_[i][j] << endl;
                exchange_corellation_matrix_eigen_(j,i) = exchange_corellation_matrix_[i][j];
            }
        }
    }
}

void Energy::mock_Set_exchange_corellation_matrix(vector<Basis_function> basis_set, vector<vector<float> > density_matrix) {
    exchange_corellation_matrix_eigen_.resize(basis_set.size(),basis_set.size());
    exchange_corellation_matrix_.resize(basis_set.size());
    for (int row = 0; row < basis_set.size(); row++) exchange_corellation_matrix_[row].resize(basis_set.size());

    ExchangeCorrelation XC_object = ExchangeCorrelation(basis_set, density_matrix);
    auto start = std::chrono::system_clock::now();
    for (int i = 0; i < basis_set.size(); i++) {
        for (int j = 0; j < basis_set.size(); j++) {
            if (j >= i) {
                exchange_corellation_matrix_[i][j] = 0; //TODO
                exchange_corellation_matrix_[j][i] = exchange_corellation_matrix_[i][j];
                // cout << "element value is:\t" << exchange_corellation_matrix_[i][j] << endl;
                exchange_corellation_matrix_eigen_(j,i) = exchange_corellation_matrix_[i][j];
            }
        }
        auto end_element = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed = end_element - start;
        // std::cout << "Elapsed time for exchange_row: " << elapsed.count() << "s" << endl;
    }
}

void Energy::Create_density_matrix(int size) {
    density_matrix_eigen_.resize(size, size);
    density_matrix_.resize(size);
    for (int i = 0; i < size; i++) density_matrix_[i].resize(size);
}

void Energy::Set_density_matrix(vector<vector<float> > orbital_coefficients, int electrons) {

    for (int i = 0; i < density_matrix_.size(); i++) {
        for (int j = 0; j < density_matrix_.size(); j++) {
            density_matrix_[i][j] = 0.0;
            density_matrix_eigen_(i,j) = 0.0;
        }
    }

    for (int electron = 0; electron < electrons/2; electron++) {
        for (int i = 0; i < density_matrix_.size(); i++) {
            for (int j = 0; j < density_matrix_.size(); j++) {
                density_matrix_[i][j] += 2*orbital_coefficients[electron][i] * orbital_coefficients[electron][j];
                density_matrix_eigen_(i,j) = density_matrix_[i][j];
            }
        }
    }
}

void Energy::Set_density_matrix(Eigen::MatrixXf orbital_coefficients, int electrons) {
    for (int i = 0; i < density_matrix_.size(); i++) {
        for (int j = 0; j < density_matrix_.size(); j++) {
            density_matrix_[i][j] = 0.0;
            density_matrix_eigen_(i,j) = 0;
        }
    }

    for (int electron = 0; electron < electrons/2; electron++) {
        for (int i = 0; i < density_matrix_.size(); i++) {
            for (int j = 0; j < density_matrix_.size(); j++) {
                density_matrix_[i][j] += 2*orbital_coefficients(electron, i) * orbital_coefficients(electron, j);
                density_matrix_eigen_(i,j) = density_matrix_[i][j];
            }
        }
    }
}

float Energy::get_total_energy(Eigen::MatrixXf hamiltonian) {
    // """Calculates the total energy for restricted hartree fock methods.

    // Parameters
    // ----------
    // density_matrix : np.matrix
    // hamiltonian : np.matrix

    // Returns
    // -------
    // total_energy : float

    // """
    // length = density_matrix.shape[0]
    float total_energy = 0.0;
        for (int i = 0; i < density_matrix_.size(); i++) {
            for (int j = 0; j < density_matrix_.size(); j++) {
                total_energy += 1.0/2.0 * density_matrix_[i][j] * (core_hamiltonian_matrix_[i][j] + hamiltonian(i, j));
                //  cout << "density_matrix_[i][j]\t" << density_matrix_[i][j] <<  "\tcore_hamiltonian_matrix_[i][j] " << core_hamiltonian_matrix_[i][j] << "\thamiltonian(i, j) " << hamiltonian(i, j) << endl;
            }
        }
        // cout << "total_energy\t" << total_energy << endl;
    return total_energy;
}

float Energy::get_total_energy(vector<vector<float> >  hamiltonian) {

    float total_energy = 0.0;
        for (int i = 0; i < density_matrix_.size(); i++) {
            for (int j = 0; j < density_matrix_.size(); j++) {
                total_energy += 1/2 * density_matrix_[i][j] * core_hamiltonian_matrix_[i][j] + hamiltonian[i][j];
            }
        }
    return total_energy;
}

void Energy::get_Fock_matrix(Eigen::MatrixXf Fock_matrix){
    float matrix_element = 0.0;
    for (int i = 0; i < density_matrix_.size(); i++) {
        for (int j = 0; j < density_matrix_.size(); j++) {
            for (int a = 0; a < density_matrix_.size(); a++) {
                for (int b = 0; b < density_matrix_.size(); b++) {
                    matrix_element += density_matrix_[i][j] * repulsion_tensor_[i][j][a][b];
                }
            }
            matrix_element += exchange_corellation_matrix_[i][j] + core_hamiltonian_matrix_[i][j];
            Fock_matrix(i,j) = matrix_element;
            matrix_element = 0.0;
        }
    }
}

Energy::~Energy() {
    // cout << coulomb_law_matrix_[19].size() << endl;
    // cout  << coulomb_law_matrix_[19][19] << endl;
}