import matplotlib.pyplot as plt
import sys, os, math as m
import numpy as np 
import scipy as sp
from scipy.integrate import quad, dblquad
import scipy.special as spsp

def get_local_spherical_coordinates(R, Phi, Teta, r, phi, teta):
    cos_teta = np.cos(teta)
    cos_Teta = np.cos(Teta)
    cos_phi = np.cos(phi)
    sin_Teta = np.sin(Teta)
    sin_teta = np.sin(teta)
    R_phi_sq = r*cos_teta*r*cos_teta + R*R - 2*R*r*cos_teta*cos_phi
    R_phi = np.sqrt(R_phi_sq)
    # delta_Phi = np.arccos( (R*R + R_phi_sq - 2*r*cos_teta*r*cos_teta)/(R*R_phi) )
    Teta_res = round(np.arctan( (R_phi*sin_Teta + r*np.sin(teta)) / R_phi*cos_Teta), 4)
    cos_Teta_res = np.cos(Teta_res)
    arccos_argument = round((R*R*cos_Teta*cos_Teta + R_phi_sq*cos_Teta_res*cos_Teta_res - r*r*cos_teta*cos_teta)/(2*R*R_phi*cos_Teta*cos_Teta_res), 4)
    print(arccos_argument)
    delta_Phi = np.arccos( arccos_argument   )
    R_res = np.sqrt(R_phi_sq*cos_Teta*cos_Teta + (R_phi*sin_Teta + r*sin_teta)*(R_phi*sin_Teta + r*sin_teta))
    return R_res, Phi+delta_Phi, Teta_res


print(get_local_spherical_coordinates(100, np.pi*10/180, np.pi*10/180, 1, -np.pi, np.pi*1/180))
# print(get_local_spherical_coordinates(100, 0, 0, 1, -np.pi, np.pi*1/180))


