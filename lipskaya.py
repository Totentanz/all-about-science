import numpy as np 
import scipy as sp
import matplotlib.pyplot as plt
import sys, os, math as m
from multiprocessing import Pool
import brandt
import multiprocessing as mp
from scipy.integrate import quad, dblquad


vk = 0.468
alpha = 5
beta = 6
r_max = 1.42
r_step = 1000
q = 1

def Euler(n = 50, h = 0.01, x = 1, y = 1):
    for i in range(n):
        r = 1 - i*h
        o = 1/r
        l = np.log(o)
        m = r * Function_Q(l)

        # y += h * function(x, y)
        # x += h
    return x, y # решение

def Function_Q(z):
    vq = 0
    nq = 200
    hq = z/nq

    for k in range( nq):
        sq = np.sqrt(vq)
        vq = vq + hq * function_u(sq)

    return vq

def function_u(s):
    tau = 0
    vu = 1

    nu = 200
    hu = s/nu

    for k in range(1, nu):
        tau = hu * k
        vu = vu + hu * 2 * (vk / vu + tau)

    return vu

def function(x, y):
    return 6 * x**2 + 5 * x * y # функция первой производной

# nr = 990
# hr = 0.001
# W = []
# Q = []
# R = []
# om = 0

# # while (m.fabs(om - 1.25) > 0.005):
# W = []
# Q = []
# R = []
# for i in range(0, nr):
#     vr = 1 - hr*i
#     R.append(vr)
#     vo = 1 / vr
#     vl = np.log(vo)
#     om = vr * Function_Q(vl)
#     W.append(om)
# print(vr, om )

