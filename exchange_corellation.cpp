#include "exchange_corellation.hpp"


ExchangeCorrelation::ExchangeCorrelation(vector<Basis_function> basis_set, vector<vector<float> > density_matrix):
                    basis_set_(basis_set),
                    density_matrix_(density_matrix)
{
    
}

float ExchangeCorrelation::Calculate_Slater_Exchange(float density){

        float alpha = 0.6666;
    return -(3.0/2.0) * alpha * pow((3.0/M_PI),(1.0/3.0)) * pow(density, float(1.0/3.0));
}

float ExchangeCorrelation::wigner_seitz_radius(float density) {

    return pow((3 / (4 * M_PI * density)), float(1.0/3.0));
}

float ExchangeCorrelation::VoskoWilkNusair_potential(float density) {
        // """Returns the value of the Vosko-Wilk-Nusair correlation potential for a given density and fit parameters.

        // Parameters
        // ----------
        // density : float

        // Returns
        // -------
        // : float

    // Attributes
    // ----------
    // a : float
    //     VWN3 paramagnetic = 0.0621814
    //     VWN5 paramagnetic = 0.0621814
    //     VWN3 ferromagnetic = 0.0310907
    //     VWN5 ferromagnetic = 0.0310907
    // x_0 : float
    //     VWN3 paramagnetic = -0.409286
    //     VWN5 paramagnetic = -0.10498
    //     VWN3 ferromagnetic = -0.743294
    //     VWN5 ferromagnetic = -0.32500
    // b : float
    //     VWN3 paramagnetic = 13.0720
    //     VWN5 paramagnetic = 3.72744
    //     VWN3 ferromagnetic = 20.1231
    //     VWN5 ferromagnetic = 7.06042
    // c : float
    //     VWN3 paramagnetic = 42.7198
    //     VWN5 paramagnetic = 12.9352
    //     VWN3 ferromagnetic = 101.578
    //     VWN5 ferromagnetic = 18.0578

        // """

        float a=0.0621814;
        float x_0=-0.409286;
        float b=13.0720;
        float c=42.7198;
    float result = 0.0;
    if (density == 0) {
        result = 0;
    } else {    
        
        float x_2 = wigner_seitz_radius(density);
        float x = sqrt(x_2);
        float x_x = x_2 + b * x + c;
        float x_x_0 = x_0*x_0 + b * x_0 + c;
        float q = sqrt(4 * c - b*b);

        result =  a * (log(x_2 / x_x) + (2 * b / q) * atan(q / (2 * x + b))
        - (b * x_0 / x_x_0) * (log((x - x_0)*(x - x_0) / x_x) + (2 * (b + 2 * x_0) / q) * atan(q / (2 * x + b))));
    }
        return result;
}

float ExchangeCorrelation::electron_density(float x, float y, float z){
    float density = 0.0;
    for (int i = 0; i< basis_set_.size(); i++) {
        for (int j = 0; j< basis_set_.size(); j++) {
            if (i == j) {
                density += density_matrix_[i][j] * basis_set_[i].value(x, y, z)*basis_set_[i].value(x, y, z);
            } else {
            if (i < j) density += 2 * basis_set_[i].value(x, y, z) * density_matrix_[i][j] * basis_set_[j].value(x, y, z);
            }
        }
    }
    return density;
}

double ExchangeCorrelation::integrand(int i, int j, float x, float y, float z)
// float ExchangeCorrelation::integrand(ExchangeCorrelation* instance, int i, int j, float x, float y, float z)
{
    // float x = rho * sin(theta) * cos(phi);
    // float y = rho * sin(theta) * sin(phi);
    // float z = rho * cos(theta);
    float density = electron_density(x, y, z);
    float value_1 = basis_set_[i].value(x, y, z);
    float value_2 = basis_set_[j].value(x, y, z);
    float exchange = Calculate_Slater_Exchange(density);
    float potential = VoskoWilkNusair_potential(density);
    // float radius_vector = x*x + y*y + z*z;
    // cout <<"i " << i << "\tj " << j << "\tvalue_1 " << value_1 << "\tvalue_2 " << value_2 << "\texchange " << exchange << "\tpotential " << potential << endl;
    // return  instance->basis_set_[i].value(x, y, z) * (instance->Calculate_Slater_Exchange(instance->electron_density(x, y, z))
    // + instance->VoskoWilkNusair_potential(instance->electron_density(x, y, z))) * instance->basis_set_[j].value(x, y, z);
    // + instance->VoskoWilkNusair_potential(0)) * instance->basis_set_[j].value(x, y, z);
    return (exchange + potential) * value_1 * value_2;
}

double integrand(ExchangeCorrelation* instance, int i, int j, float x, float y, float z)
// float ExchangeCorrelation::integrand(ExchangeCorrelation* instance, int i, int j, float x, float y, float z)
{
    // float x = rho * sin(theta) * cos(phi);
    // float y = rho * sin(theta) * sin(phi);
    // float z = rho * cos(theta);
    float density = instance->electron_density(x, y, z);
    float value_1 = instance->basis_set_[i].value(x, y, z);
    float value_2 = instance->basis_set_[j].value(x, y, z);
    float exchange = instance->Calculate_Slater_Exchange(density);
    float potential = instance->VoskoWilkNusair_potential(density);
    // float radius_vector = x*x + y*y + z*z;
    // cout <<"i " << i << "\tj " << j << "\tvalue_1 " << value_1 << "\tvalue_2 " << value_2 << "\texchange " << exchange << "\tpotential " << potential << endl;
    // return  instance->basis_set_[i].value(x, y, z) * (instance->Calculate_Slater_Exchange(instance->electron_density(x, y, z))
    // + instance->VoskoWilkNusair_potential(instance->electron_density(x, y, z))) * instance->basis_set_[j].value(x, y, z);
    // + instance->VoskoWilkNusair_potential(0)) * instance->basis_set_[j].value(x, y, z);
    return (exchange + potential) * value_1 * value_2;
}

double _gsl_wrap_Z (double x, void* params)
// double ExchangeCorrelation::_gsl_wrap_Z (double x, void* params)
{
	gsl_params_XC* p = (gsl_params_XC*) params;
    float result = integrand((ExchangeCorrelation*) (p->instance), p->i, p->j, p->x, p->y, x);
    // cout << "integrand element\t" << p->i << " " << p->j << "\tvalue\t" << result << endl;
    return result;
}

double _gsl_wrap_Y (double x, void* params)
// double ExchangeCorrelation::_gsl_wrap_Y (double x, void* params)
{
	double result_qagp;
	double result_qagiu;
	double error;
    size_t oyaeboo;
	gsl_params_XC* p = (gsl_params_XC*) params;
	p->y = x;

	gsl_integration_workspace *w_qagp =  gsl_integration_workspace_alloc(20);
	gsl_integration_workspace *w_qagiu =  gsl_integration_workspace_alloc(20);
    gsl_integration_cquad_workspace *w_cquad = gsl_integration_cquad_workspace_alloc(20);
    double points[3] = {-15.,0.0,15.};
    gsl_function gsl_wrap_Z; 
    gsl_wrap_Z.function = _gsl_wrap_Z;
    gsl_wrap_Z.params = p;


	// gsl_integration_qag (&gsl_wrap_Z, -25., 25., 0, 1e-5,20, GSL_INTEG_GAUSS21, w_qagp, &result_qagp, &error);
    // gsl_integration_qagp (&gsl_wrap_Z, points, 3, 0, 1e-5,20, w_qagp, &result_qagp, &error);
    gsl_integration_cquad(&gsl_wrap_Z, -3.0, 3.0, 0, 1e-5, w_cquad,  &result_qagp, &error, &oyaeboo);
    // gsl_integration_qagi(&gsl_wrap_Z, 0, 1e-4,20, w_qagp, &result_qagp, &error);
    // cout << "integration over Z for element\t" << p->i << " " << p->j << "\tvalue\t" << result_qagp << endl;

gsl_integration_cquad_workspace_free(w_cquad);
	gsl_integration_workspace_free (w_qagp);

	return result_qagp;
}

double _gsl_wrap_X (double x, void* params)
// double ExchangeCorrelation::_gsl_wrap_X (double x, void* params)
{
	double result_qagp;
	double result_qagiu;
	double error;
    size_t oyaeboo;
	gsl_params_XC* p = (gsl_params_XC*) params;
	p->x = x;
	gsl_integration_workspace *w_qagp =  gsl_integration_workspace_alloc(20);
    gsl_integration_cquad_workspace *w_cquad = gsl_integration_cquad_workspace_alloc(20);
    double points[3] = {-15.,0.0,15.};
    gsl_function gsl_wrap_Y; 
    gsl_wrap_Y.function = _gsl_wrap_Y;
    gsl_wrap_Y.params = p;
    ExchangeCorrelation* instance_ = (ExchangeCorrelation*)(p-> instance);
    float x_i = instance_->basis_set_[p->i].coordinates_[1];
    float x_j = instance_->basis_set_[p->j].coordinates_[1];
    float max_x = max(x_i, x_j);
    float min_x = min(x_i, x_j);
	// gsl_integration_qagp (&gsl_wrap_Y, points, 3, 0, 1e-5,20, w_qagp, &result_qagp, &error);
    gsl_integration_cquad(&gsl_wrap_Y, min_x-2.5, max_x+2.5, 0, 1e-5, w_cquad,  &result_qagp, &error, &oyaeboo);
    // gsl_integration_qag (&gsl_wrap_Y, -25., 25., 0, 1e-5,20, GSL_INTEG_GAUSS21, w_qagp, &result_qagp, &error);
    // gsl_integration_qagi(&gsl_wrap_Y, 0, 1e-4,20, w_qagp, &result_qagp, &error);
    // cout << "integration over Y for element\t" << p->i << " " << p->j << "\tvalue\t" << result_qagp << endl;

gsl_integration_cquad_workspace_free(w_cquad);
	gsl_integration_workspace_free (w_qagp);

	return result_qagp;
}

double ExchangeCorrelation::get_integral(int i, int j) {

    gsl_params_XC first_params = {.instance = (void*)(this),
                                  .i = i, \
                                  .j = j};
    double result_qagp;
    float distance = coordinate_distance(basis_set_[i].coordinates_, basis_set_[j].coordinates_);

	// double result_qagiu;
	double error;
    size_t oyaeboo;
	// gsl_params_XC* p = (gsl_params_XC*) params;
	// p->x = x;
	gsl_integration_workspace *w_qagp =  gsl_integration_workspace_alloc(20);
    gsl_integration_cquad_workspace *w_cquad = gsl_integration_cquad_workspace_alloc(20);
    double points[3] = {-15.,0.0,15.};
    gsl_function gsl_wrap_X;
    gsl_wrap_X.function = _gsl_wrap_X;
    gsl_wrap_X.params = &first_params;
    float max_x = max(basis_set_[i].coordinates_[0], basis_set_[j].coordinates_[0]);
    float min_x = min(basis_set_[i].coordinates_[0], basis_set_[j].coordinates_[0]);

    if ((distance > 2.0) ) {
        // cout << "start to count exchange" << endl;
        result_qagp = 0.0;
    } else {
        if ((distance < 1) | (basis_set_[j].marker_ == 4)) {
            auto start = std::chrono::system_clock::now();
            cout << "start to count exchange" << i << "\t" << j << endl;

	        // gsl_integration_qagp (&gsl_wrap_X, points, 3, 0, 1e-5,20, w_qagp, &result_qagp, &error);
            gsl_integration_cquad(&gsl_wrap_X, min_x-2.5, max_x+2.5, 0, 1e-5, w_cquad,  &result_qagp, &error, &oyaeboo);
            // gsl_integration_qag (&gsl_wrap_X, -25., 25., 0, 1e-5,20, GSL_INTEG_GAUSS21, w_qagp, &result_qagp, &error);
            // gsl_integration_qagi(&gsl_wrap_X, 0, 1e-4,20, w_qagp, &result_qagp, &error);
            cout << "integration over X for element\t" << i << " " << j << "\tvalue\t" << result_qagp << endl;
                        auto end_element = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed = end_element - start;
    std::cout << "Elapsed time for " << i << "\t" << j << " integral: " << elapsed.count() << "s" << endl;
        }
    }
    gsl_integration_cquad_workspace_free(w_cquad);
	gsl_integration_workspace_free (w_qagp);

	return result_qagp;
}

double ExchangeCorrelation::get_dummy_integral(int i, int j) { 
         
    double a = -3.0;
    double b = 15.0;
    float max_x = max(basis_set_[i].coordinates_[0], basis_set_[j].coordinates_[0]) + 2.5;
    float min_x = min(basis_set_[i].coordinates_[0], basis_set_[j].coordinates_[0]) - 2.5;
    float max_y = max(basis_set_[i].coordinates_[1], basis_set_[j].coordinates_[1]) + 2.5;
    float min_y = min(basis_set_[i].coordinates_[1], basis_set_[j].coordinates_[1]) - 2.5;
    int m = 50;
    double a_z = -3.0;
    double b_z = 3.0;
    int z_m;
   double h;
   double h_x=(max_x-min_x)/float(m);
   double h_y = (max_x-min_x)/float(m);
   double h_z = (b_z-a_z)/float(m);
//    z_m = int((b_z - a_z)/h);

    float distance = coordinate_distance(basis_set_[i].coordinates_, basis_set_[j].coordinates_);
   double I, I2 = 0, I4 = 0;
   I4 = integrand(i, j, a + h, a + h, a_z + h);
    if ((distance > 2.0) ) {
        // cout << "start to count exchange" << endl;
        I = 0.0;
    } else {
        if ((distance < 1) | (basis_set_[j].marker_ == 4)) {
        // cout << "start to count exchange" << endl;

        cout << "start to count exchange\t" << i << "\t" << j << endl;
        auto start = std::chrono::system_clock::now();
            for(int x_k = 2; x_k < m; x_k += 2 ) {
                for(int y_k = 2; y_k < m; y_k += 2 ) {
                    for(int z_k = 2; z_k < m; z_k += 2 ) {
                            I4 += integrand(i, j, a + (x_k+1)*h_x, a + (y_k+1)*h_y, a_z + (z_k+1)*h_z);
                            I2 += integrand(i, j, a + (x_k)*h_x, a + (y_k)*h_y, a_z + (z_k)*h_z);
                    }
                    I = integrand(i, j, a, a, a_z ) + integrand(i, j, b, a + (y_k)*h_y, b_z );
                    // auto end_row = std::chrono::system_clock::now();
                    // std::chrono::duration<double> elapsed_row = end_row - start;
                    // cout << "one y row get " << elapsed_row.count() << " s" << endl;
                }
                I = integrand(i, j, a, a, a_z ) + integrand(i, j, a + (x_k+1)*h_x, b, b_z );
            }

            auto end_element = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed = end_element - start;
    std::cout << "Elapsed time for " << i << "\t" << j << " integral: " << elapsed.count() << "s" << endl;
       I = integrand(i, j, a, a, a_z ) + integrand(i, j, b, b, b_z ) + 4*I4 + 2*I2;
   I *= h_x/3.0*h_y/3.0*h_z/3.0;
   cout << "result for " << i << "\t" << j << "\tis\t" << I << endl;
    }
    }

    // auto end_element = std::chrono::system_clock::now();
    // std::chrono::duration<double> elapsed = end_element - start;
    // std::cout << "Elapsed time for this integral: " << elapsed.count() << "s" << endl;
 
   return I;

}