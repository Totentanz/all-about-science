import numpy as np 
import scipy as sp
import matplotlib.pyplot as plt
import sys, os, math as m
from multiprocessing import Pool
import brandt
import multiprocessing as mp
import multiprocessing.managers
from scipy.integrate import quad, dblquad
import time
start_time = time.time()
m=mp.Manager()

alpha = 5
Z = -6
r_max = 1.42
r_step = 1000
t_step = 200 # step for cos(teta)
q_eff = 3.14
l = 1
epsilon = 0.000000001
mixing_koefficient = 1/1024
plank2 = 0.29247
rho    = 1/brandt.a0
plank  = np.sqrt(plank2)
gamma_  = np.sqrt(plank)*rho
alpha_     = alpha / plank2
Z_    = Z / plank2
orbital_    = l*(l+1)
# Z_    = Z 
# orbital_    = l*(l+1)*plank2

h = r_max/(r_step + 1)
ht = 2/t_step
R = np.zeros(r_step) # массив координат

complex_zero = np.complex(0,0)
cr = np.complex(1,0)
ci = np.complex(0,1)

phi1 = np.zeros(r_step, dtype=np.complex_)
phi2 = np.zeros(r_step, dtype=np.complex_)

undisturbed_pot = np.zeros(r_step)
yukawa_pot = np.zeros(r_step)
yukawa_disturbance_martix = np.zeros((r_step, r_step))


def get_R(res, i):
    value = 0.5*h*(2*i+1)
    res.set(value)

def get_undisturbed_potential(res, r):
    res.set(Z_/r - orbital_ / (2*r*r))
    # res.set(Z_ / r)

def get_R_between_vectors(res, ri, rj, t):
    res.set(ri*ri + rj*rj - 2*ri*rj*t)

def scalar_multiplication(F, wave_f, R):
    result = 0
    for i in range(r_step):
        result += F[i]*wave_f[i]*R[i]*R[i]*h
    return result

result = 0
result_wrapper = multiprocessing.Manager().Value('f', result)

# заполняем массив координат
for i in range(1000):
    # result_wrapper = multiprocessing.Manager().Value('f', R[i])
    reader_process = multiprocessing.Process(target=get_R, args=(result_wrapper, i))
    reader_process.start()
    reader_process.join()
    R[i] = result_wrapper.get()


# заполняем массив с кулоновским и орбитальным потенциалом
for i in range(1000):
    current_r = R[i]
    reader_process = multiprocessing.Process(target=get_undisturbed_potential, args=(result_wrapper, current_r))
    reader_process.start()
    reader_process.join()
    undisturbed_pot[i] = result_wrapper.get()

# plt.plot(range(r_step), R)
# plt.plot(range(r_step), undisturbed_pot)
# plt.show()

def get_inhomogenity(res, ri,rj):
    current_res = 0
    for k in range(1, t_step):
        tk = -1 + 0.5 * ht * (2*k-1)
        R_for_grin = np.sqrt(ri*ri + rj*rj - 2*ri*rj*tk)
        E0 = np.exp(-gamma_*R_for_grin)
        current_res += 0.5 * (E0 / R_for_grin) * ht
    res.set(current_res)
    return current_res

# for j in range(r_step):
#     r_j = R[j]
#     for i in range(j):
#         r_i = R[i]
#         yukawa_inhomogenity = 0
#         reader_process = multiprocessing.Process(target=get_inhomogenity, args=(result_wrapper, r_i, r_j))
#         processes.append(reader_process)
#         reader_process.start()
#         reader_process.join()
#         yukawa_inhomogenity = result_wrapper.get()
#         yukawa_disturbance_martix[i][j] = yukawa_inhomogenity
#         yukawa_disturbance_martix[j][i] = yukawa_inhomogenity
#     print(j)
#     print("--- %s seconds ---" % (time.time() - start_time))
# print(yukawa_disturbance_martix[:][:])



for j in range(r_step):
    r_j = R[j]
    for i in range(j):
        r_i = R[i]
        yukawa_inhomogenity = 0
        for k in range(1, t_step):
            tk = -1 + 0.5 * ht * (2*k-1)
            R_for_grin = np.sqrt(r_i*r_i + r_j*r_j - 2*r_i*r_j*tk)
            E0 = np.exp(-gamma_*R_for_grin)
            yukawa_inhomogenity += 0.5 * (E0 / R_for_grin) * ht
        yukawa_disturbance_martix[i][j] = yukawa_inhomogenity
        yukawa_disturbance_martix[j][i] = yukawa_inhomogenity
    print(j)
    print("--- %s seconds ---" % (time.time() - start_time))

# print(yukawa_disturbance_martix[:][:])

wave_function_p = np.ones(r_step, dtype=np.complex_)
wave_function_k = np.ones(r_step, dtype=np.complex_)

# # начинаем итерации

iteration = 0
# c
# 
eigen_energy = np.complex(0,0)
# c
def probe_function(r):
    # return np.exp(-r)/np.sqrt(2)
    return brandt.RadialHWf(r,2,1,q)
    # return 1/(r_max*r)
    
# заполняем массив с значениями волновой функции
for i in range(r_step-1):
    ri = R[i]
    s1 = np.exp(-gamma_*ri) / ri  # wtf?!
    wave_function_p[i] = cr * s1
wave_function_p[r_step-1] = complex_zero

# нормируем волновую функцию
wave_function_p_norm = 1/np.sqrt(np.linalg.norm(wave_function_p*R))

print('wave_function_p_norm', wave_function_p_norm)

wave_function_p *= wave_function_p_norm
wave_function_k = np.copy(wave_function_p)

# SOMEWHERE HERE

full_potential = []

for i in range(r_step):
    ri = R[i]
    s0 = 0
    for j in range(r_step):
        rj = R[j]
        dr = rj * rj * h
        wave_f_real = np.real(wave_function_p[j])
        wave_f_image = np.imag(wave_function_p[j])
        pj = wave_f_real*wave_f_real + wave_f_image*wave_f_image 
        s1 = yukawa_disturbance_martix[i][j] * pj
        s0 = s0 + s1 * dr
    
    yukawa_pot[i] = alpha_ * s0

    gi = undisturbed_pot[i] - yukawa_pot[i]
    full_potential.append(gi)
    with open("potential_"+ str(iteration) + '.dat', 'w') as fp:
        for pot in full_potential:
            fp.write(str(float(pot)) + '/n')
    if i == 0:
        s2 = (ri + 0.5*h) / ri
        cm = 0
        cp = (wave_function_p[i] - wave_function_p[i+1])*s2*s2
    if i == r_step-1:
        s1 = (ri - 0.5*h) / ri
        cm = (wave_function_p[i] - wave_function_p[i-1])*s1*s1
        cp = 0
    else:
        s1 = (ri - 0.5*h) / ri
        s2 = (ri + 0.5*h) / ri
        cm = (wave_function_p[i] - wave_function_p[i-1])*s1*s1
        cp = (wave_function_p[i] - wave_function_p[i+1])*s2*s2
    phi1[i] = cp + cm + wave_function_p[i] * gi * h*h
    phi2[i] = phi1[i] - eigen_energy * wave_function_p[i]

phi1[r_step-1] = complex_zero
phi2[r_step-1] = complex_zero

phi2_norm = np.sqrt(np.linalg.norm(phi2*R))
print ('iteration', iteration, 'eigen_energy', eigen_energy, 'phi2_norm', phi2_norm)
print("--- %s seconds ---" % (time.time() - start_time))

# if number of iterations much than 1000 or norm less than eps
if phi2_norm > epsilon:

    iteration += 1
    full_potential = []

    eigen_energy_updated = scalar_multiplication(phi1, wave_function_p, R)
    print('eigen_energy_updated', eigen_energy_updated)   
    if np.absolute(eigen_energy_updated) < 0:
        print("WAT")
    wave_function_k = wave_function_p / eigen_energy_updated
    wave_function_k_norm = 1/np.sqrt(np.linalg.norm(wave_function_k*R))
    wave_function_k *= wave_function_k_norm
    eigen_energy = eigen_energy * (1-mixing_koefficient) + eigen_energy_updated * mixing_koefficient
    wave_function_p = wave_function_p*(1-mixing_koefficient) + wave_function_k * mixing_koefficient
    wave_function_p_norm = 1/np.sqrt(np.linalg.norm(wave_function_p*R))

    print('wave_function_p_norm', 1/wave_function_p_norm)

    wave_function_p *= wave_function_p_norm

    print(' new wave_function_p_norm', np.sqrt(np.linalg.norm(wave_function_p*R)))
    print("--- %s seconds ---" % (time.time() - start_time))

    for i in range(r_step):
        ri = R[i]
        s0 = 0
        for j in range(r_step):
            rj = R[j]
            dr = rj * rj * h
            wave_f_real = np.real(wave_function_p[j])
            wave_f_image = np.imag(wave_function_p[j])
            pj = wave_f_real*wave_f_real + wave_f_image*wave_f_image 
            s1 = yukawa_disturbance_martix[i][j] * pj
            s0 = s0 + s1 * dr
        
        yukawa_pot[i] = alpha_ * s0

        gi = undisturbed_pot[i] - yukawa_pot[i]
        full_potential.append(gi)
        if i == 0:
            s2 = (ri + 0.5*h) / ri
            cm = 0
            cp = (wave_function_p[i] - wave_function_p[i+1])*s2*s2
        if i == r_step-1:
            s1 = (ri - 0.5*h) / ri
            cm = (wave_function_p[i] - wave_function_p[i-1])*s1*s1
            cp = 0
        else:
            s1 = (ri - 0.5*h) / ri
            s2 = (ri + 0.5*h) / ri
            cm = (wave_function_p[i] - wave_function_p[i-1])*s1*s1
            cp = (wave_function_p[i] - wave_function_p[i+1])*s2*s2
        phi1[i] = cp + cm + wave_function_p[i] * gi * h*h
        phi2[i] = phi1[i] - eigen_energy * wave_function_p[i]
    with open("potential_"+ str(iteration) + ".dat", 'w') as fp:
        for i in full_potential:
            fp.write(str(float(i)) + '/n')

    phi1[r_step-1] = complex_zero
    phi2[r_step-1] = complex_zero

    phi2_norm = np.sqrt(np.linalg.norm(phi2*R))
    print ('iteration', iteration, 'eigen_energy', eigen_energy, 'phi2_norm', phi2_norm)







print("--- %s seconds ---" % (time.time() - start_time))