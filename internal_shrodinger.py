import numpy as np 
import scipy as sp
import matplotlib.pyplot as plt
import sys, os, math as m
from multiprocessing import Pool
import brandt
import multiprocessing as mp
from scipy.integrate import quad, dblquad

alpha = 5
beta = -6
r_max = 1.42
r_step = 500
q = 3.14

h = r_max/(r_step + 1)


def R_for_Grin(r, rr,tau):
    distance_between_vec = (np.sqrt(r*r + rr*rr - 2*r*rr*tau))
    return distance_between_vec

def get_Yukawa_inhomogenity(ri, rj):
    # angle_step = m.pi/180
    result = 0
    for tau in range(0, r_step):
        tau = tau/r_step
        r = R_for_Grin(ri, rj, tau)
        result += np.exp(-r/brandt.a0)*angle_step*angle_step/r
    return result/2

def wrapper(r, rj, wf, result):
    # r - radius for current point, rj - point of space, wf - wave function in rj
    # WF = brandt.RadialHWf(rj,2,1,alpha)
    inhomogenity = get_Yukawa_inhomogenity(r, rj)*wf*wf*rj*rj*h
    result.value += inhomogenity
    return inhomogenity


def potential(r, wave_func):
    inhomogenity = 0
    mul_inhomo = mp.Value('f')
    mul_inhomo_result = mp.Value('f')
    with Pool(processes=5) as pool:
        for x in range(0, r_step):
            rj = r_max*x/r_step
            pool.apply_async(wrapper, args=(r, rj, wave_func[x], mul_inhomo_result))

    return -(beta/r + 2/r/r + alpha*mul_inhomo_result.value)
    # with Pool(10) as pool:
    #     for x in range(1, r_step):
    #         rj = r_max*x/r_step
    #         WF = brandt.RadialHWf(rj,2,1,1)
    #         inhomogenity += get_Yukawa_inhomogenity(r, rj)*WF*WF*rj*r_max/r_step
    # return - beta/r + alpha*inhomogenity

def probe_function(r):
    # return np.exp(-r)/np.sqrt(2)
    return brandt.RadialHWf(r,2,1,q)
    # return 1/(r_max*r)

# wave_function = np.zeros(r_step)
# wave_function[0] = probe_function(h/2)
# for i in range (1, r_step-1):
#         wave_function[i] = probe_function(h*i)
# R = []
# Potential = []
# Brandt_potential = []
# with open('potentials_mul.dat', 'w') as fp:
#     for r in range(50, r_step):
#         r = r*r_max/r_step
#         R.append(r)
#         P = potential(r,wave_function)*brandt.Ry
#         B = brandt.Brandt(r, beta,1,1, 1, brandt.a0)*brandt.Ry
#         Potential.append(P)
#         Brandt_potential.append(B)
#         print("r is ", r, "potentials are", P, B)
#         fp.write(str(r) + ', ' + str(P) + ', ' + str(B) + '\n')


# FullEnergy = quad(lambda x:potential(x, wave_function)*brandt.RadialHWf(x, 2,1,q)*brandt.RadialHWf(x, 2,1,q)*x*x, 0, np.inf)
# print(FullEnergy[0]*brandt.Ry)
# print(brandt.FullEnergy(2,1,q)*brandt.Ry)
# with open('potentials_mul.dat', 'w') as fp:
#     for r in enumerate(R):
#         fp.write(str(r) + ', ' + str(Potential[r[0]]) + ', ' + str(Brandt_potential[r[0]]) + '\n')

# plt.plot(R, Potential, 'r')
# plt.plot(R, Brandt_potential, 'black')
# plt.legend(['ourPotential', 'Brandt porential'], loc='upper right')
# plt.savefig("potentials_mul.pdf", dpi=300, format='pdf')


# rtol = 1e-05
# atol = 1e-08
# # R_vector = np.ones(r_step, )
# G = np.ones((r_step, r_step))
# g_scalar = 0
# pot = []
# with open ('G.dat', 'w') as fp:
#     for i in range(0, r_step-1):
#         g_scalar = 0
#         pot[i] = potential(h*i)
#         for j in range(0, r_step-1):
#             G[i][j] = wrapper(h*i, h*j)
#             g_scalar += G[i][j]
#             fp.write( str(round(G[i][j], 4)) + '\t\t')
#         fp.write('\n\n')
#         # print(g_scalar)
#         print(beta/h*i)
# norm = np.linalg.norm(G)
# print(np.allclose(G, G.T, rtol=rtol, atol=atol))
# print(norm)
# print(g_scalar)

wave_function = np.zeros(r_step)
wave_function[0] = probe_function(h/2)
for i in range (1, r_step):
    wave_function[i] = probe_function(h*i)
wave_function[r_step-1] = 0
left_part = np.zeros((r_step,r_step))
left_part[0][0] =  (h*(0.5)/h)*(h*(0.5)/h) - h*h*potential(h, wave_function)
left_part[0][1] =  -(h*(0.5)/h)*(h*(0.5)/h)
# eigen_energies = np.ones((1, r_step))
eigen_energy = 0
norm = np.inf
F = np.ones(r_step)
s = 0
with open('wave_function_' + str(s) +'.dat', 'w') as fp:
    for value in wave_function:
        fp.write(str(value) + '\n')
# print(h)
with open('left_part_mul.dat', 'w') as fp:
    for i in range (1, r_step-1):
        left_part[i][i-1] = -(h*(i-0.5)/(h*i))*(h*(i-0.5)/(h*i))
        left_part[i][i+1] = -(h*(i+0.5)/(h*i))*(h*(i+0.5)/(h*i))
        left_part[i][i] = (h*(i-0.5)/(h*i))*(h*(i-0.5)/(h*i)) + (h*(i+0.5)/(h*i))*(h*(i+0.5)/(h*i)) - h*h*potential(h*i, wave_function)
        fp.write(str(left_part[i][i-1]) + ', ' + str(left_part[i][i]) + ', ' + str(left_part[i][i+1]) + '\n')
    left_part[left_part.shape[0]-1][left_part.shape[0]-2] = -(h*(i+0.5)/(h*r_step))*(h*(i+0.5)/(h*r_step))
    left_part[left_part.shape[0]-1][left_part.shape[0]-1] = (h*(r_step-0.5)/(h*r_step))*(h*(r_step-0.5)/(h*r_step)) - h*h*potential(r_max, wave_function)
    # for i in range(left_part.shape[0]):
    #     print(i, left_part[i][i])

    F = np.dot(left_part, wave_function)
    shape = F.shape
    print(shape)
    # F[-1] = 2*F[-2] - F[-3]
    # F[0] = 2*F[1] - F[2]
    # F[1] = 2*F[2] - F[3]
    # F[2] = 2*F[3] - F[4]
    
    # plt.plot(range(r_step), F - wave_function*eigen_energy, color='black')
    # plt.plot(range(r_step), wave_function)
    # plt.show()
    norm = np.linalg.norm(F - wave_function*eigen_energy)

    print(norm)
    # print(det)

while(norm > 0.00000001):
    s += 1
    # eigen_energies = F*wave_function
    eigen_energy = 0
    for i in range (0, r_step-1):
        eigen_energy += F[i]*wave_function[i]*h*h*i
    wave_function = F/eigen_energy
    wave_norm = np.linalg.norm(wave_function)
    wave_function /= wave_norm
    wave_function[r_step-1] = 0
    with open('wave_function_' + str(s) +'.dat', 'w') as fp:
        for value in wave_function:
            fp.write(str(value) + '\n')
    left_part[0][0] =  (h*(0.5)/h)*(h*(0.5)/h) - h*h*potential(h, wave_function)
    # left_part[0][1] =  -2*(h*(0.5)/h)*(h*(0.5)/h)

    for i in range (1, r_step-1):
        left_part[i][i-1] = -(h*(i-0.5)/(h*i))*(h*(i-0.5)/(h*i))
        left_part[i][i+1] = -(h*(i+0.5)/(h*i))*(h*(i+0.5)/(h*i))
        left_part[i][i] = (h*(i-0.5)/(h*i))*(h*(i-0.5)/(h*i)) + (h*(i+0.5)/(h*i))*(h*(i+0.5)/(h*i)) - h*h*potential(h*i, wave_function)
        # fp.write(str(left_part[i][i-1]) + ', ' + str(left_part[i][i]) + ', ' + str(left_part[i][i+1]) + '\n')
    # left_part[left_part.shape[0]-1][left_part.shape[0]-2] = -2*(h*(i+0.5)/(h*r_step))*(h*(i+0.5)/(h*r_step))
    left_part[left_part.shape[0]-1][left_part.shape[0]-1] = (h*(r_step-0.5)/(h*r_step))*(h*(r_step-0.5)/(h*r_step)) - h*h*potential(r_max, wave_function)

    F = np.dot(left_part, wave_function)
    # F[-1] = 2*F[-2] - F[-3]
    # F[0] = 2*F[1] - F[2]
    # F[1] = 2*F[2] - F[3]
    # F[2] = 2*F[3] - F[4]
    with open('F_' + str(s) +'.dat', 'w') as fp:
        for value in F:
            fp.write(str(value) + '\n')
    norm = np.linalg.norm(F - wave_function*eigen_energy)
    print('iteration number', s)
    print('norm', norm)
    print('eigen_energy', eigen_energy)
