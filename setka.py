import numpy as np 
import scipy as sp
import matplotlib.pyplot as plt
import sys, os, math as m
from multiprocessing import Pool
import brandt, lipskaya
import multiprocessing as mp
from scipy.integrate import quad, dblquad

alpha = 5
beta = 6
r_max = 1.42
r_step = 1000
q = 1

def probe_function(r):
    return np.exp(-r)/np.sqrt(2)

h = r_max/(r_step + 1)
R = []
for i in range (r_step):
    R.append(h*(i+0.5))


