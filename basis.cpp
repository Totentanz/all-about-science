#include <iostream>
#include <math.h>
#include <complex>
#include <fstream>
#include <string>
#include <vector>
#include "basis.hpp"

long double fact(int N)
{
    if(N < 0) // если пользователь ввел отрицательное число
        return 1; // возвращаем ноль
    if (N == 0) // если пользователь ввел ноль,
        return 1; // возвращаем факториал от нуля - не удивляетесь, но это 1 =)
    else // Во всех остальных случаях
        return N * fact(N - 1); // делаем рекурсию.
}

Basis_function::Basis_function() {
    contraction_ = {0};
    exponent_ = {0};
    coordinates_ = {0,0,0};
    integral_exponents_ = {0,0,0};
    gaussian_norms_ = {1,1,1};
    marker_ = 0;
    normalisation_ = 0.0;
}
Basis_function::~Basis_function() { };

// bool Basis_function::operator<(Basis_function func) {
//     cout << "first_exponent is " << func.exponent_[0] << " mine is " << exponent_[0] << endl;
//     if (func.marker_ > marker_) {
//         return true;
//     } else {
//         return false;
//     }
// }
bool Basis_function::operator<(Basis_function func) {
    // cout << "first_exponent is " << func.exponent_[0] << " mine is " << exponent_[0] << endl;
    float first_sum = func.exponent_[0] + func.exponent_[1]*20 + func.exponent_[2]*30 \
    + func.coordinates_[0] + func.coordinates_[1]*20+ func.coordinates_[2]*30 \
     + func.integral_exponents_[0] + func.integral_exponents_[1]*20 + func.integral_exponents_[2]*30 \
     + func.contraction_[0] + func.contraction_[1]*20 + func.contraction_[2]*30;

    float second_sum = exponent_[0] + exponent_[1]*20 + exponent_[2]*30 \
        + coordinates_[0] + coordinates_[1] + coordinates_[2] \
    + integral_exponents_[0] + integral_exponents_[1]*20 + integral_exponents_[2] *30 +\
     contraction_[0] + contraction_[1]*20 + contraction_[2]*30;
    // cout << "first_sum \t" << first_sum << "\tsecond_sum\t" << second_sum << endl;
    // cout << "func.marker_ \t" << func.marker_ << "\tmarker_\t" << marker_ << endl;
    if (first_sum > second_sum) {
        return true;
    } else {
        return false;
    }
}

Basis_function::Basis_function(string orbital, vector<float> coordinates = {0,0,0}, float integral_ex = 0, int marker = 0) {
    marker_ = marker;
    // cout << "i'm here!" << endl;
    if (orbital == "1s") {
        contraction_ = {0.15432897, 0.53532814, 0.44463454};
        exponent_ = {71.6168370, 13.0450960, 3.5305122};
        // cout << "exponent_ size is \t" << exponent_.size() << endl;
        coordinates_ = coordinates;
        integral_exponents_ = {0.0,0.0,0.0};
        for (int gaussian = 0; gaussian < exponent_.size(); gaussian++) {
            float out1 = fact(2 * integral_exponents_[0] - 1) * fact(2 * integral_exponents_[1] - 1) * fact(2 * integral_exponents_[2] - 1);
            // cout << "out1\t" << out1 << endl;
            float out2 = sqrt((M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian])));
            // cout << "out1\t" << out2 << endl;
            float out3 = pow((4 *exponent_[gaussian]),(integral_exponents_[0] + integral_exponents_[1] + integral_exponents_[2]));
            // cout << "out1\t" << out3 << endl;
            float norm = 1 / sqrt((out1 * out2) / out3);
            // cout << "norm\t" << norm << endl;
            gaussian_norms_.push_back(norm);
        }
    }
        if (orbital == "P1s") {
        contraction_ = {0.00183470, 0.01403730, 0.06884260, 0.23218440, 0.46794130, 0.36231200};
        exponent_ = { 3047.52490000, 457.36951000, 103.94869000, 29.21015500, 9.28666300, 3.16392700};
        // cout << "exponent_ size is \t" << exponent_.size() << endl;
        coordinates_ = coordinates;
        integral_exponents_ = {0.0,0.0,0.0};
        for (int gaussian = 0; gaussian < exponent_.size(); gaussian++) {
            float out1 = fact(2 * integral_exponents_[0] - 1) * fact(2 * integral_exponents_[1] - 1) * fact(2 * integral_exponents_[2] - 1);
            // cout << "out1\t" << out1 << endl;
            float out2 = sqrt((M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian])));
            // cout << "out1\t" << out2 << endl;
            float out3 = pow((4 *exponent_[gaussian]),(integral_exponents_[0] + integral_exponents_[1] + integral_exponents_[2]));
            // cout << "out1\t" << out3 << endl;
            float norm = 1 / sqrt((out1 * out2) / out3);
            // cout << "norm\t" << norm << endl;
            gaussian_norms_.push_back(norm);
        }
    }
    if (orbital == "2s") {
        contraction_ = {-0.09996723 , 0.39951283, 0.70011547};
        exponent_ = {2.9412494, 0.6834831, 0.2222899};
        coordinates_ = coordinates;
        integral_exponents_ = {0.0,0.0,0.0};
        for (int gaussian = 0; gaussian < exponent_.size(); gaussian++) {
            float out1 = fact(2 * integral_exponents_[0] - 1) * fact(2 * integral_exponents_[1] - 1) * fact(2 * integral_exponents_[2] - 1);
            float out2 = sqrt((M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian])));
            float out3 = pow((4 *exponent_[gaussian]),(integral_exponents_[0] + integral_exponents_[1] + integral_exponents_[2]));
            float norm = 1 / sqrt((out1 * out2) / out3);
            // cout << "norm\t" << norm << endl;
            gaussian_norms_.push_back(norm);
        }
    }
        if (orbital == "P2s") {
        contraction_ = {-0.11933240 , -0.16085420, 1.14345640};
        exponent_ = {7.86827240, 1.88128850, 0.54424930};
        coordinates_ = coordinates;
        integral_exponents_ = {0.0,0.0,0.0};
        for (int gaussian = 0; gaussian < exponent_.size(); gaussian++) {
            float out1 = fact(2 * integral_exponents_[0] - 1) * fact(2 * integral_exponents_[1] - 1) * fact(2 * integral_exponents_[2] - 1);
            float out2 = sqrt((M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian])));
            float out3 = pow((4 *exponent_[gaussian]),(integral_exponents_[0] + integral_exponents_[1] + integral_exponents_[2]));
            float norm = 1 / sqrt((out1 * out2) / out3);
            // cout << "norm\t" << norm << endl;
            gaussian_norms_.push_back(norm);
        }
    }
    if (orbital == "2p") {
        // cout << "i'm here!" << endl;
        contraction_ = {0.15591627, 0.60768372, 0.39195739};
        exponent_ = {2.9412494, 0.6834831, 0.2222899};
        coordinates_ = coordinates;
        integral_exponents_[integral_ex] = 1;
        for (int gaussian = 0; gaussian < exponent_.size(); gaussian++) {
            float out1 = fact(2 * integral_exponents_[0] - 1) * fact(2 * integral_exponents_[1] - 1) * fact(2 * integral_exponents_[2] - 1);
            float out2 = sqrt((M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian])));
            float out3 = pow((4 *exponent_[gaussian]),(integral_exponents_[0] + integral_exponents_[1] + integral_exponents_[2]));
            float norm = 1 / sqrt((out1 * out2) / out3);
            // cout << "norm\t" << norm << endl;
            gaussian_norms_.push_back(norm);
        }
    }
        if (orbital == "P2p") {
        // cout << "i'm here!" << endl;
        contraction_ = {0.06899910, 0.31642400, 0.74430830};
        exponent_ = {7.86827240, 1.88128850, 0.54424930};
        coordinates_ = coordinates;
        integral_exponents_[integral_ex] = 1;
        for (int gaussian = 0; gaussian < exponent_.size(); gaussian++) {
            float out1 = fact(2 * integral_exponents_[0] - 1) * fact(2 * integral_exponents_[1] - 1) * fact(2 * integral_exponents_[2] - 1);
            float out2 = sqrt((M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian])));
            float out3 = pow((4 *exponent_[gaussian]),(integral_exponents_[0] + integral_exponents_[1] + integral_exponents_[2]));
            float norm = 1 / sqrt((out1 * out2) / out3);
            // cout << "norm\t" << norm << endl;
            gaussian_norms_.push_back(norm);
        }
    }
        if (orbital == "P3s") {
        // cout << "i'm here!" << endl;
        contraction_ = {1.0};
        exponent_ = {0.16871440};
        coordinates_ = coordinates;
        integral_exponents_[integral_ex] = 1;
        for (int gaussian = 0; gaussian < exponent_.size(); gaussian++) {
            float out1 = fact(2 * integral_exponents_[0] - 1) * fact(2 * integral_exponents_[1] - 1) * fact(2 * integral_exponents_[2] - 1);
            float out2 = sqrt((M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian])));
            float out3 = pow((4 *exponent_[gaussian]),(integral_exponents_[0] + integral_exponents_[1] + integral_exponents_[2]));
            float norm = 1 / sqrt((out1 * out2) / out3);
            // cout << "norm\t" << norm << endl;
            gaussian_norms_.push_back(norm);
        }
    }
        if (orbital == "P3p") {
        // cout << "i'm here!" << endl;
        contraction_ = {1.0};
        exponent_ = {0.16871440};
        coordinates_ = coordinates;
        integral_exponents_[integral_ex] = 1;
        for (int gaussian = 0; gaussian < exponent_.size(); gaussian++) {
            float out1 = fact(2 * integral_exponents_[0] - 1) * fact(2 * integral_exponents_[1] - 1) * fact(2 * integral_exponents_[2] - 1);
            float out2 = sqrt((M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian])));
            float out3 = pow((4 *exponent_[gaussian]),(integral_exponents_[0] + integral_exponents_[1] + integral_exponents_[2]));
            float norm = 1 / sqrt((out1 * out2) / out3);
            // cout << "norm\t" << norm << endl;
            gaussian_norms_.push_back(norm);
        }
    }
        if (orbital == "P3d") {
        // cout << "i'm here!" << endl;
        contraction_ = {1.0};
        exponent_ = {0.80000000};
        coordinates_ = coordinates;
        integral_exponents_[integral_ex] = 2;
        for (int gaussian = 0; gaussian < exponent_.size(); gaussian++) {
            float out1 = fact(2 * integral_exponents_[0] - 1) * fact(2 * integral_exponents_[1] - 1) * fact(2 * integral_exponents_[2] - 1);
            float out2 = sqrt((M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian]))*(M_PI / (2 *exponent_[gaussian])));
            float out3 = pow((4 *exponent_[gaussian]),(integral_exponents_[0] + integral_exponents_[1] + integral_exponents_[2]));
            float norm = 1 / sqrt((out1 * out2) / out3);
            // cout << "norm\t" << norm << endl;
            gaussian_norms_.push_back(norm);
        }
    }
}

Basis_function::Basis_function(float contraction, float exponent, vector<float> coordinates, vector<float> integral_exponents, float normalisation):
    coordinates_(coordinates),
    integral_exponents_(integral_exponents),
    normalisation_(normalisation)
{
    contraction_.resize(1);
    contraction_[0] = contraction;
    exponent_.resize(1);
    exponent_[0] = exponent;
    marker_ = 0;
    gaussian_norms_ = {1,1,1};

}

void Basis_function::Reset(float contraction, float exponent, vector<float> coordinates, vector<float> integral_exponents, float normalisation)
{
    coordinates_ = coordinates;
    integral_exponents_ = integral_exponents;
    normalisation_ = normalisation;
    contraction_.resize(1);
    contraction_[0] = contraction;
    exponent_.resize(1);
    exponent_[0] = exponent;
    marker_ = 0;
}

void Basis_function::add_function(float contraction, float exponent) {
    contraction_.push_back(contraction);
    exponent_.push_back(exponent);
}

void Basis_function::normalize() {
    if (contraction_.size() == 1) {
        normalisation_ == 1;
    } else {
        // cout << "trouble_here\n" << endl;
        float result = 0.0;
        for (int i = 0; i < contraction_.size(); i++) {
            // cout << "trouble_here1\t"<< exponent_.size() << endl;
            float exp_1 = exponent_[i];
            // cout << "trouble_here1.1\n" << endl;
            float contr_1 = contraction_[i];
            // cout << "trouble_here1.2\n" << endl;
            float norm_1 = gaussian_norms_[i];
            for (int j = 0; j < contraction_.size(); j++) {
                // cout << i << "\t" << j << endl;
                float exp_2 = exponent_[j];
                float contr_2 = contraction_[j];
                float norm_2 = gaussian_norms_[j];

                float out1 = fact(2 * integral_exponents_[0] - 1) * fact(2 * integral_exponents_[1] - 1) * fact(2 * integral_exponents_[2] - 1);
                
                float out2 = pow((M_PI / (exp_1 + exp_2)),(3 / 2));
                // cout << "trouble_here2\n" << endl;
                float out3 = pow((2 * (exp_1 + exp_2)) ,(integral_exponents_[0] + integral_exponents_[1] + integral_exponents_[2]));
                // cout << "trouble_here3\n" << endl;
                result += (contr_1 * contr_2 * norm_1 * norm_2 * out1 * out2) / out3;
                // self.normalisation_memo = 1 / sqrt(ans)
            }
            
            normalisation_ = 1 / sqrt(abs(result));
            // cout << "normalisation_ " << normalisation_ << endl;

        }
    }

}

vector<float> gaussian_product_coordinate(float exp_1, vector<float> coordinates_1, float exp_2, vector<float> coordinates_2)
{
    vector<float> result(3);
    result[0] = (exp_1 * coordinates_1[0] + exp_2 * coordinates_2[0]) / (exp_1 + exp_2);
    result[1] = ((exp_1 * coordinates_1[1] + exp_2 * coordinates_2[1]) / (exp_1 + exp_2));
    result[2] = ((exp_1 * coordinates_1[2] + exp_2 * coordinates_2[2]) / (exp_1 + exp_2));
    // cout << "result\t" << result[0] << "\t" << result[1] << "\t" << result[2] << "\t" << endl;
    return result;
}

vector<float> vector_minus(vector<float> a, vector<float> b) {
    vector<float> result(3);
    result[0] = a[0] - b[0];
    result[1] = a[1] - b[1];
    result[2] = a[2] - b[2];
    return result;
}

float combination(float n, float k){
    float result = 0;
    if (k <= n)  result = fact(n) / (fact(k) * fact(n - k));
    return result;
}

float binomial_coefficient(int j, float integral_exponent_1, float integral_exponent_2, float a, float b) {
     float result = 0;
    //  cout << "i'm here!!!lol" << endl;
    for (int k = max(0, j - int(integral_exponent_2)); k <  min(j, int(integral_exponent_1)) + 1; k++) \
        result += combination(integral_exponent_1, k) * combination(integral_exponent_2, j - k) *  pow(a,(integral_exponent_1 - k)) * pow(b,(integral_exponent_2 + k - j));
    return result;
}

float s_function(float l_1, float l_2,  float a, float b, float g) {
    float s = 0.0;
    // cout << "i'm here!!!lol" << endl;
    for (int j = 0; j < (((l_1 + l_2) / 2) + 1); j++) \
        s += binomial_coefficient(2 * j, l_1, l_2, a, b) * (fact(2 * j - 1) / pow((2 * g), j));
    
    return s;
}

float orbital_overlap(float exp_1, vector<float> coord_1, vector<float> integral_exp_1, float exp_2, vector<float> coord_2, vector<float> integral_exp_2)
{
    
    float distance = sqrt(  (coord_1[0] - coord_2[0])*(coord_1[0] - coord_2[0]) + (coord_1[1] - coord_2[1])*(coord_1[1] - coord_2[1]) + (coord_1[2] - coord_2[2])*(coord_1[2] - coord_2[2]) );
    //  cout << "distance\t" << distance  << endl;
    vector<float> gaussian_product_coordinates = gaussian_product_coordinate(exp_1, coord_1, exp_2, coord_2);
    // cout << "gaussian_product_coordinates\t" << gaussian_product_coordinates[0]  << endl;
    vector<float> p_a = vector_minus(gaussian_product_coordinates, coord_1);
    vector<float> p_b = vector_minus(gaussian_product_coordinates, coord_2);
    

    float result_exp = exp_1 + exp_2;
    float s_x = s_function(integral_exp_1[0], integral_exp_2[0], p_a[0], p_b[0], result_exp);
    float s_y = s_function(integral_exp_1[1], integral_exp_2[1], p_a[1], p_b[1], result_exp);
    float s_z = s_function(integral_exp_1[2], integral_exp_2[2], p_a[2], p_b[2], result_exp);
    
    float s_ij = pow((M_PI / result_exp), (3.0/2.0)) * exp(- exp_1 * exp_2 * distance*distance / result_exp) * s_x * s_y * s_z;
    return s_ij;
}

vector<Basis_function> del_operator(float exponent, vector<float> coord, vector<float> integral_exp, float normalisation) {
    vector<Basis_function> array_of_primitives(0);
    // Basis_function array_of_primitives = Basis_function();
    float contraction = exponent * ((2 * (integral_exp[0] + integral_exp[1] + integral_exp[2])) + 3);
    Basis_function primitive_basis = Basis_function(contraction, exponent, coord, integral_exp, 1);
    array_of_primitives.push_back(primitive_basis);
    // array_of_primitives.add_function(contraction, exponent);
    contraction = - 2 * pow(exponent, 2);
    primitive_basis = Basis_function(contraction, exponent, coord, {integral_exp[0] +2, integral_exp[1], integral_exp[2]}, normalisation );
    array_of_primitives.push_back(primitive_basis);

    primitive_basis = Basis_function(contraction, exponent, coord, {integral_exp[0] +2, integral_exp[1], integral_exp[2]}, normalisation );
    array_of_primitives.push_back(primitive_basis);

    primitive_basis = Basis_function(contraction, exponent, coord, {integral_exp[0] +2, integral_exp[1], integral_exp[2]}, normalisation );
    array_of_primitives.push_back(primitive_basis);

    if (integral_exp[0] >= 2) {
        contraction = - (1/2) * (integral_exp[0] * (integral_exp[0] - 1));
        primitive_basis = Basis_function(contraction, exponent, coord, {integral_exp[0] - 2, integral_exp[1], integral_exp[2]}, normalisation );
        array_of_primitives.push_back(primitive_basis);
    }
    if (integral_exp[1] >= 2) {
        contraction = - (1/2) * (integral_exp[1] * (integral_exp[1] - 1));
        primitive_basis = Basis_function(contraction, exponent, coord, {integral_exp[0], integral_exp[1] - 2, integral_exp[2]}, normalisation );
        array_of_primitives.push_back(primitive_basis);
    }
    if (integral_exp[2] >= 2) {
        contraction = - (1/2) * (integral_exp[2] * (integral_exp[2] - 1));
        primitive_basis = Basis_function(contraction, exponent, coord, {integral_exp[0], integral_exp[1], integral_exp[2] - 2}, normalisation );
        array_of_primitives.push_back(primitive_basis);
    }

    return array_of_primitives;
}

float a_function(int l, int r, int i, float l_1, float l_2, float pa, float pb, float pc, float g){
    float e = 1 / (4*g);
    float out1 = binomial_coefficient(l, l_1, l_2, pa, pb);
    float out2 = pow((-1),i) * fact(l) * pow(pc, (l - 2*r - 2*i)) * pow(e,(r + i));
    float out3 = fact(r) * fact(i) * fact(l - 2*r - 2*i);
    float ans = pow((-1), l) * out1 * (out2/out3);
    return ans;
}

float boys_function(float v, float x){
    float result = 0.0;
    int i = 0;
    // # Approximation of the boys function for small x
    if (x <= 25) {
        float g_v = tgamma(v + 0.5);
        while (true){
            float seq = (g_v / tgamma(v + i + 1.5)) * pow(x,i);
            if (seq < float(1e-10)) break;
            result += seq;
            i += 1;
        }
        
        result *= (1/2) * exp(-x);
        // cout << "x\t" << x << endl;

    // # Approximation of the boys function for large x
    } else {
        float g_v = tgamma(v + 0.5);
        while (true){
            float seq = (g_v / tgamma(v - i + 1.5)) * pow(x,-i);
            if (seq < float(1e-10)) break;
            result += seq;
            i += 1;
        }
        result *= (1/2) * exp(-x);
        result = (g_v / (2* pow(x,(v + 0.5)))) - result;
    }
    // cout << "boys_function\t" << result << endl;
        return result;
}

float nuclear_attraction(float exp_1, vector<float> coord_1, vector<float> integral_exp_1, float exp_2, vector<float> coord_2, vector<float> integral_exp_2, \
                 vector<float> nuclei_coordinates)
{
    vector<float> gaussian_product_coordinates = gaussian_product_coordinate(exp_1, coord_1, exp_2, coord_2);
    float gaussian_centers_distance = sqrt((coord_1[0] - coord_2[0])*(coord_1[0] - coord_2[0])\
                                        + (coord_1[1] - coord_2[1])*(coord_1[1] - coord_2[1]) \
                                        + (coord_1[2] - coord_2[2])*(coord_1[2] - coord_2[2]) );
    float gaussian_nuclei_distance = sqrt((gaussian_product_coordinates[0] - nuclei_coordinates[0])*(gaussian_product_coordinates[0] - nuclei_coordinates[0]) \
                                        + (gaussian_product_coordinates[1] - nuclei_coordinates[1])*(gaussian_product_coordinates[1] - nuclei_coordinates[1]) \
                                        + (gaussian_product_coordinates[2] - nuclei_coordinates[2])*(gaussian_product_coordinates[2] - nuclei_coordinates[2]) );
    // cout << "gaussian_product_coordinates\t" << gaussian_product_coordinates[0] << "\t" << gaussian_product_coordinates[1] << "\t" << gaussian_product_coordinates[2] << "\t" << endl;

    vector<float> product_to_f_gaussian = vector_minus(gaussian_product_coordinates, coord_1);
    vector<float> product_to_s_gaussian = vector_minus(gaussian_product_coordinates, coord_2);
    vector<float> product_to_nuclei = vector_minus(gaussian_product_coordinates, nuclei_coordinates);

    float result_exp = exp_1 + exp_2;

    float ans = 0.0;
    for (int l = 0; l < (integral_exp_1[0] + integral_exp_2[0] + 1); l++) {
        for (int r = 0; r < (int(l/2) + 1); r++) {
            for (int i =0; i<  (int((l - 2*r) / 2) + 1); i++ ) {
                float out1 = a_function(l, r, i, integral_exp_1[0], integral_exp_2[0], product_to_f_gaussian[0], product_to_s_gaussian[0], product_to_nuclei[0], result_exp);
                for (int m = 0; m < (integral_exp_1[1] + integral_exp_2[1] + 1); m++) {
                    for (int s = 0; s < (int(m/2) + 1); s++) {
                        for (int j = 0; j < (int((m - 2*s) / 2) + 1); j++) {
                            float out2 = a_function(m, s, j, integral_exp_1[1], integral_exp_2[1], product_to_f_gaussian[1], product_to_s_gaussian[1], product_to_nuclei[1], result_exp);
                            for (int n = 0; n < (integral_exp_1[2] + integral_exp_2[2] + 1); n++) {
                                for (int t = 0; t < (int(n/2) + 1); t++) {
                                    for (int k = 0; k < (int((n - 2*t) / 2) + 1); k++){
                                        float out3 = a_function(n, t, k, integral_exp_1[2], integral_exp_2[2], product_to_f_gaussian[2], product_to_s_gaussian[2], product_to_nuclei[2], result_exp);
                                        float v = (l + m + n) - 2*(r + s + t) - (i + j + k);
                                        // cout << "v\t" << result_exp * pow(gaussian_nuclei_distance, 2) << endl;
                                        float out4 = boys_function(v, result_exp * pow(gaussian_nuclei_distance, 2));
                                        float out5 = out1 * out2 * out3 * out4;
                                        ans += out5;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    ans *= ((2 * M_PI) / result_exp) * exp(- (exp_1 * exp_2 * gaussian_centers_distance*gaussian_centers_distance) / result_exp);
    // cout << "nuclear_attraction element \t"<< ans << endl;
    return ans;
}

float coordinate_distance(vector<float> coord_1, vector<float> coord_2) {
    return sqrt((coord_1[0] - coord_2[0])*(coord_1[0] - coord_2[0])\
                + (coord_1[1] - coord_2[1])*(coord_1[1] - coord_2[1]) \
                + (coord_1[2] - coord_2[2])*(coord_1[2] - coord_2[2]) );
}

float Basis_function::value(float x, float y, float z) {
    float result = 0.0;
    for (int i = 0; i< exponent_.size(); i++) {
        result += gaussian_norms_[i] * contraction_[i] * pow((x - coordinates_[0]), integral_exponents_[i]) * pow((y - coordinates_[1]), integral_exponents_[i]) \
        * pow((z - coordinates_[2]), integral_exponents_[i]) * exp(-exponent_[i] \
        * ((x - coordinates_[0])*(x - coordinates_[0]) + (y - coordinates_[1])*(y - coordinates_[1]) + (z - coordinates_[2])*(z - coordinates_[2])));
    }
    return result;
}