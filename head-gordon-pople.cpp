#include "head-gordon-pople.hpp"

using namespace std;


HGP_Get_Value::HGP_Get_Value() {
        a_7_ = 0.0;
        r_7_ = {};
        end_dict_ = {}; 
    };

// float a_7 = 0.0;
// vector<float> r_7 = {};
// map<int, float> end_dict = {};

static int os_count(int i){
    if (i == 0){
        return 1;
    } else {
        return i;
    }
}

float HGP_Get_Value::hgp_begin_horizontal(Basis_function &g1, Basis_function &g2, Basis_function &g3, Basis_function &g4){

    vector<float> l_2 = g2.integral_exponents_;
    vector<float> l_4 = g4.integral_exponents_;
    // cout << "voltmeter 2\t" << l_2[0] << endl;
    if (l_2[0] > 0){
        int new_int = 0;
        return horizontal_recursion(new_int, g1, g2, g3, g4);
    } else {
        if (l_2[1] > 0){
            int new_int = 1;
            return horizontal_recursion(new_int, g1, g2, g3, g4);
        } else {
            if (l_2[2] > 0){
                int new_int = 2;
                return horizontal_recursion(new_int, g1, g2, g3, g4);
            } else {
                if (l_4[0] > 0){
                    int new_int = 0;
                    return horizontal_recursion(new_int, g3, g4, g1, g2);
                } else {
                    if (l_4[1] > 0){
                        int new_int = 1;
                        return horizontal_recursion(new_int, g3, g4, g1, g2);
                    } else {
                        if (l_4[2] > 0){
                            int new_int = 2;
                            return horizontal_recursion(new_int, g3, g4, g1, g2);
                        } else {
                            int new_int = 0;
                            return hgp_begin_vertical(new_int, g1, g2, g3, g4);
                        }
                    }
                }
            }
        }
    }
}

float HGP_Get_Value::horizontal_recursion(int& r, Basis_function& g1, Basis_function& g2, Basis_function& g3, Basis_function& g4){
    // Basis_function g1a1 = Basis_function();
    // Basis_function g2m1 = Basis_function();
    Basis_function g1a1;
    Basis_function g2m1;
    float out2 = 0.0;

    float d_1 = g1.contraction_[0];
    float d_2 = g2.contraction_[0];
    float a_1 = g1.exponent_[0];
    float a_2 = g2.exponent_[0];
    vector<float> r_1 = g1.coordinates_;
    vector<float> r_2 = g2.coordinates_;
    vector<float> l_1 = g1.integral_exponents_;
    vector<float> l_2 = g2.integral_exponents_;

    if (r == 0) {
        g1a1.Reset(d_1, a_1, r_1, {l_1[0] + 1, l_1[1], l_1[2]}, g1.normalisation_);
        g2m1.Reset(d_2, a_2, r_2, {l_2[0] - 1, l_2[1], l_2[2]}, g2.normalisation_);
    } else {
        if (r == 1){
            // cout << "d_1\t" << d_1 << endl;
            // cout << "a_1\t" << a_1 << endl;
            // cout << "r_1\t" << r_1.size() << "\t" << r_1[0] << endl;
            // cout << "l_1\t" << l_1[0] << endl;
            // cout << "l_1[1] + 1\t" << l_1[1] + 1 << endl;
            // cout << " l_1[2]\t" <<  l_1[2] << endl;
            // cout << "g1.normalisation_\t" << g1.normalisation_ << endl;
            g1a1.Reset(d_1, a_1, r_1, {l_1[0], l_1[1] + 1, l_1[2]}, g1.normalisation_);
            g2m1.Reset(d_2, a_2, r_2, {l_2[0], l_2[1] - 1, l_2[2]}, g2.normalisation_);
            } else {
                if (r == 2){
                    g1a1.Reset(d_1, a_1, r_1, {l_1[0], l_1[1], l_1[2] + 1}, g1.normalisation_);
                    g2m1.Reset(d_2, a_2, r_2, {l_2[0], l_2[1], l_2[2] - 1}, g2.normalisation_);
                }
            }
    }

    float out1 = hgp_begin_horizontal(g1a1, g2m1, g3, g4);
    if (r_1[r] != r_2[r]) {
        out2 = (r_1[r] - r_2[r]) * hgp_begin_horizontal(g1, g2m1, g3, g4);
    }
    return out1 + out2;
}

float HGP_Get_Value::hgp_begin_vertical(int& m, Basis_function& g1, Basis_function& g2, Basis_function& g3, Basis_function& g4){
    vector<float> l_1 = g1.integral_exponents_;
    vector<float> l_3 = g3.integral_exponents_;
    // cout << "voltmeter 1" << endl;
    if (l_1[0] > 0){
        int new_int = 0;
        float new_float = float(new_int);
        vector<Basis_function> vertical_factory = hgp_vertical_factory(new_int, g1, g2, g3, g4);
        return vertical_recursion(new_int, m, vertical_factory);
    }
    if (l_3[0] > 0){
        int new_int = 0;
        float new_float = float(new_int);
        vector<Basis_function> vertical_factory = hgp_vertical_factory(new_int, g3, g4, g1, g2);
        return vertical_recursion(new_int, m, vertical_factory);
    }
    if (l_1[1] > 0){
        int new_int = 1;
        float new_float = float(new_int);
        vector<Basis_function> vertical_factory = hgp_vertical_factory(new_int, g1, g2, g3, g4);
        return vertical_recursion(new_int, m, vertical_factory);
    }
    if (l_3[1] > 0){
        int new_int = 1;
        float new_float = float(new_int);
        vector<Basis_function> vertical_factory = hgp_vertical_factory(new_int, g3, g4, g1, g2);
        return vertical_recursion(new_int, m, vertical_factory);
    }
    if (l_1[2] > 0){
        int new_int = 2;
        float new_float = float(new_int);
        vector<Basis_function> vertical_factory = hgp_vertical_factory(new_int, g1, g2, g3, g4);
        return vertical_recursion(new_int, m, vertical_factory);
    }
    if (l_3[2] > 0){
        int new_int = 2;
        float new_float = float(new_int);
        vector<Basis_function> vertical_factory = hgp_vertical_factory(new_int, g3, g4, g1, g2);
        return vertical_recursion(new_int, m, vertical_factory);
    }else{
        return end_dict_[m];
    }
}

float HGP_Get_Value::vertical_recursion(int& r, int& m, vector<Basis_function>& Basis_function_vector){
    Basis_function g1 = Basis_function_vector[0];
    Basis_function g2 = Basis_function_vector[1];
    Basis_function g3 = Basis_function_vector[2];
    Basis_function g4 = Basis_function_vector[3];
    Basis_function g5 = Basis_function_vector[4];
    Basis_function g6 = Basis_function_vector[5];
    float out1 = 0.0;
    float out2 = 0.0;
    float out3 = 0.0;
    float out4 = 0.0;
    float out5 = 0.0;

    float a_1 = g1.exponent_[0];
    float a_3 = g3.exponent_[0];
    float a_2 = g2.exponent_[0];
    float a_4 = g4.exponent_[0];
    float a_5 = a_1 + a_2;
    float a_6 = a_3 + a_4;

    vector<float> r_1 = g1.coordinates_;
    vector<float> r_2 = g2.coordinates_;
    vector<float> r_5 = gaussian_product_coordinate(a_1, r_1, a_2, r_2);

    if (r_5[r] != r_1[r]){
        out1 = (r_5[r] - r_1[r]) * hgp_begin_vertical(m, g1, g2, g3, g4);
    }
    if (r_7_[r] != r_5[r]){
        int new_m = m+1;
        out2 = (r_7_[r] - r_5[r]) * hgp_begin_vertical(new_m, g1, g2, g3, g4);
    }
    if (g5.integral_exponents_[r] >= 0){
        out3 = os_count(g1.integral_exponents_[r]) * (1 / (2 * a_5)) * hgp_begin_vertical(m, g5, g2, g3, g4);
        int new_m = m+1;
        out4 = os_count(g1.integral_exponents_[r]) * (a_7_ / (2 * a_5*a_5)) * hgp_begin_vertical(new_m, g5, g2, g3, g4);
    }
    if (g6.integral_exponents_[r] >= 0){
        int new_m = m+1;
        out5 = os_count(g3.integral_exponents_[r]) * (1 / (2*(a_5 + a_6))) * hgp_begin_vertical(new_m, g1, g2, g6, g4);
    }

    return out1 + out2 + out3 - out4 + out5;
}

vector<Basis_function> HGP_Get_Value::hgp_vertical_factory(int& r, Basis_function& g1, Basis_function& g2, Basis_function& g3, Basis_function& g4){
    vector<Basis_function> result = {};
    float d_1 = g1.contraction_[0];
    float d_3 = g3.contraction_[0];
    float a_1 = g1.exponent_[0];
    float a_3 = g3.exponent_[0];
    vector<float> r_1 = g1.coordinates_;
    vector<float> r_3 = g3.coordinates_;
    vector<float> l_1 = g1.integral_exponents_;
    vector<float> l_3 = g3.integral_exponents_;

    if (r == 0){
        Basis_function g1xm1 = Basis_function(d_1, a_1, r_1, {l_1[0] - 1, l_1[1], l_1[2]}, g1.normalisation_);
        Basis_function g1xm2 = Basis_function(d_1, a_1, r_1, {l_1[0] - 2, l_1[1], l_1[2]}, g2.normalisation_);
        Basis_function g3xm1 = Basis_function(d_3, a_3, r_3, {l_3[0] - 1, l_3[1], l_3[2]}, g3.normalisation_);
        result = {g1xm1, g2, g3, g4, g1xm2, g3xm1};
    } else {
        if (r == 1) {
            Basis_function g1ym1 = Basis_function(d_1, a_1, r_1, {l_1[0], l_1[1] - 1, l_1[2]}, g1.normalisation_);
            Basis_function g1ym2 = Basis_function(d_1, a_1, r_1, {l_1[0], l_1[1] - 2, l_1[2]}, g2.normalisation_);
            Basis_function g3ym1 = Basis_function(d_3, a_3, r_3, {l_3[0], l_3[1] - 1, l_3[2]}, g3.normalisation_);
            result = {g1ym1, g2, g3, g4, g1ym2, g3ym1}; 
        } else {
            if (r == 2) {
                Basis_function g1zm1 = Basis_function(d_1, a_1, r_1, {l_1[0], l_1[1], l_1[2] - 1}, g1.normalisation_);
                Basis_function g1zm2 = Basis_function(d_1, a_1, r_1, {l_1[0], l_1[1], l_1[2] - 2}, g2.normalisation_);
                Basis_function g3zm1 = Basis_function(d_3, a_3, r_3, {l_3[0], l_3[1], l_3[2] - 1}, g3.normalisation_);
                result = {g1zm1, g2, g3, g4, g1zm2, g3zm1};
            }
        }
    }

    return result;
}

float HGP_Get_Value::hgp_integrate( Basis_function& basis_i, Basis_function& basis_j, Basis_function& basis_k, Basis_function& basis_l){
    float result = 0.0;
    // cout << "i'm here with markers " << basis_i.marker_ << " " << basis_j.marker_ << " " << basis_k.marker_ << " " << basis_l.marker_ << endl;
    float distance_ij = coordinate_distance(basis_i.coordinates_, basis_j.coordinates_);
    float distance_ik = coordinate_distance(basis_i.coordinates_, basis_k.coordinates_);
    float distance_il = coordinate_distance(basis_i.coordinates_, basis_l.coordinates_);
    float distance_jk = coordinate_distance(basis_j.coordinates_, basis_k.coordinates_);
    float distance_jl = coordinate_distance(basis_j.coordinates_, basis_l.coordinates_);
    float distance_kl = coordinate_distance(basis_k.coordinates_, basis_l.coordinates_);
    float sum_distance = distance_kl + distance_jl + distance_jk + distance_ik + distance_il + distance_ij;

    if ((sum_distance > 2.0)  ) {
        // cout << "start to count exchange" << endl;
        result = 0.0;
    } else {
        if ((basis_i.marker_ > 3 ) | (basis_j.marker_ > 3) | ( basis_k.marker_ > 3) | (basis_l.marker_ > 3)) {
            // cout << "value " << a << " "<< b << " " << c << " " << d << " " << " is set with " << repulsion_tensor_[a][b][c][d] << endl;
            auto start = std::chrono::system_clock::now();
    float l_total = accumulate(basis_i.integral_exponents_.begin(), basis_i.integral_exponents_.end(), 0) \
                  + accumulate(basis_j.integral_exponents_.begin(), basis_j.integral_exponents_.end(), 0) \
                  + accumulate(basis_k.integral_exponents_.begin(), basis_k.integral_exponents_.end(), 0) \
                  + accumulate(basis_l.integral_exponents_.begin(), basis_l.integral_exponents_.end(), 0);

    float overall_normalisation = basis_i.normalisation_ * basis_j.normalisation_ * basis_k.normalisation_ * basis_l.normalisation_;

    vector<float> r_1 = basis_i.coordinates_;
    vector<float> r_2 = basis_j.coordinates_;
    vector<float> r_3 = basis_k.coordinates_;
    vector<float> r_4 = basis_l.coordinates_;

    int iteration = 0;
    for (int i = 0; i< basis_i.exponent_.size(); i++){
        for (int j = 0; j< basis_j.exponent_.size(); j++) {
            for (int k = 0; k< basis_k.exponent_.size(); k++){
                for (int l = 0; l< basis_l.exponent_.size(); l++) {
                    float contraction = basis_i.contraction_[i]*basis_j.contraction_[j]*basis_k.contraction_[k]*basis_l.contraction_[l] \
                                        *basis_i.gaussian_norms_[i]*basis_j.gaussian_norms_[j]*basis_k.gaussian_norms_[k]*basis_l.gaussian_norms_[l] \
                                        *overall_normalisation;
                                        // cout << "basis_i.integral_exponents_\t" << basis_j.integral_exponents_[0] << endl;
                    Basis_function g1 = Basis_function(basis_i.contraction_[i], basis_i.exponent_[i], basis_i.coordinates_, basis_i.integral_exponents_, basis_i.gaussian_norms_[i]);
                    Basis_function g2 = Basis_function(basis_j.contraction_[j], basis_j.exponent_[j], basis_j.coordinates_, basis_j.integral_exponents_, basis_j.gaussian_norms_[j]);
                    Basis_function g3 = Basis_function(basis_k.contraction_[k], basis_k.exponent_[k], basis_k.coordinates_, basis_k.integral_exponents_, basis_k.gaussian_norms_[k]);
                    Basis_function g4 = Basis_function(basis_l.contraction_[l], basis_l.exponent_[l], basis_l.coordinates_, basis_l.integral_exponents_, basis_l.gaussian_norms_[l]);

                    float c_1 = basis_i.contraction_[i];
                    float c_2 = basis_j.contraction_[j];
                    float c_3 = basis_k.contraction_[k];
                    float c_4 = basis_l.contraction_[l];
                    float n_1 = basis_i.gaussian_norms_[i];
                    float n_2 = basis_j.gaussian_norms_[j];
                    float n_3 = basis_k.gaussian_norms_[k];
                    float n_4 = basis_l.gaussian_norms_[l];


                    float a_1 = basis_i.exponent_[i];
                    float a_2 = basis_j.exponent_[j];
                    float a_3 = basis_k.exponent_[k];
                    float a_4 = basis_l.exponent_[l];
                    float a_5 = a_1 + a_2;
                    float a_6 = a_3 + a_4;
                    a_7_ = (a_5 * a_6) / (a_5 + a_6);

                    vector<float> r_5 = gaussian_product_coordinate(a_1, r_1, a_2, r_2);
                    vector<float> r_6 = gaussian_product_coordinate(a_3, r_3, a_4, r_4);
                    r_7_ = gaussian_product_coordinate(a_5, r_5, a_6, r_6);

                    float r_12 = coordinate_distance(r_1, r_2);
                    float r_34 = coordinate_distance(r_3, r_4);
                    float r_56 = coordinate_distance(r_5, r_6);

                    float boys_x = (a_5 * a_6 * r_56*r_56) / (a_5 + a_6);
                    float boys_out1 = (2 * pow(M_PI, float(5./2.))) / (a_5 * a_6 * sqrt(a_5 + a_6));
                    float boys_out2 = exp(((- a_1 * a_2 * r_12*r_12) / a_5) - ((a_3 * a_4 * r_34*r_34) / a_6));
                    float boys_out3 = boys_function(l_total, boys_x);
                    end_dict_.insert(pair<int,float>(l_total, boys_out1 * boys_out2 * boys_out3));

                    int m = int(l_total);
                    while (m >= 1) {
                        boys_out3 = (exp(-boys_x) + 2 * boys_x * boys_out3) / (2 * m - 1); //(exp(-boys_x) + 2 * boys_x * boys_out3) / (2 * m - 1)
                        m -= 1;
                        end_dict_[m] = boys_out1 * boys_out2 * boys_out3;
                    }
                    // cout << "voltmeter " << iteration << endl;
                    // cout << "basis_i.integral_exponents_\t" << g2.integral_exponents_[0] << endl;

                    result += contraction * hgp_begin_horizontal(g1, g2, g3, g4);

                    // iteration++;
                    // cout << "iteration " << iteration << endl;
                    auto end_element = std::chrono::system_clock::now();
                    std::chrono::duration<double> elapsed = end_element - start;
                    // std::cout << "Elapsed time for element: " << elapsed.count() << "s" << endl;
                }
            }
        }

    }


        }
    }
        return result;
}