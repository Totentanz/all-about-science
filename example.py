import numpy as np 
import scipy as sp
import matplotlib.pyplot as plt
import sys, os, math as m
from multiprocessing import Pool
import brandt
import multiprocessing as mp
import multiprocessing.managers
from scipy.integrate import quad, dblquad
import time
start_time = time.time()
m=mp.Manager()

alpha = 5
Z = -6
r_max = 1.42
r_step = 1000
t_step = 200 # step for cos(teta)
q_eff = 3.14
l = 1
epsilon = 0.01
mixing_koefficient = 1/1024
plank2 = 0.29247
rho    = 1/brandt.a0
plank  = np.sqrt(plank2)
gamma_  = np.sqrt(plank)*rho
alpha_     = alpha / plank2
Z_    = Z / plank2
orbital_    = l*(l+1) / plank2
lp     = 0

h = r_max/(r_step + 1)
ht = 2/t_step
R = np.zeros(r_step) # массив координат
# R = mp.Array('f',r_step, lock=False)

undisturbed_pot = np.zeros(r_step)
yukawa_pot = np.zeros(r_step)
# R = m.Array('f',range(r_step))
yukawa_disturbance_martix = np.zeros((r_step, r_step))
# заполняем массив координат

def get_R(res, i):
    value = 0.5*h*(2*i+1)
    res.set(value)

def get_undisturbed_potential(res, r):
    res.set(Z_ / r - orbital_ / np.power(r, 2))

def get_R_between_vectors(res, ri, rj, t):
    res.set(ri*ri + rj*rj - 2*ri*rj*t)

result = 0
result_wrapper = multiprocessing.Manager().Value('f', result)
# ret_value = multiprocessing.Value("d", 0.0, lock=False)
for i in range(1000):
    # result_wrapper = multiprocessing.Manager().Value('f', R[i])
    reader_process = multiprocessing.Process(target=get_R, args=(result_wrapper, i))
    reader_process.start()
    reader_process.join()
    R[i] = result_wrapper.get()


# заполняем массив с кулоновским и орбитальным потенциалом
for i in range(1000):
    current_r = R[i]
    reader_process = multiprocessing.Process(target=get_undisturbed_potential, args=(result_wrapper, current_r))
    reader_process.start()
    reader_process.join()
    undisturbed_pot[i] = result_wrapper.get()

# plt.plot(range(r_step), R)
# # plt.plot(range(r_step), undisturbed_pot)
# plt.show()

def get_inhomogenity(res, ri,rj):
    current_res = 0
    for k in range(1, t_step):
        tk = -1 + 0.5 * ht * (2*k-1)
        R_for_grin = result_wrapper.get()
        E0 = np.exp(-gamma_*R_for_grin)
        current_res += 0.5 * (E0 / R_for_grin) * ht
    res.set(current_res)

for j in range(r_step):
    r_j = R[j]
    for i in range(j):
        r_i = R[i]
        yukawa_inhomogenity = 0
        reader_process = multiprocessing.Process(target=get_undisturbed_potential, args=(result_wrapper, current_r))
        reader_process.start()
        reader_process.join()
        undisturbed_pot[i] = result_wrapper.get()


        
        for k in range(1, t_step):
            tk = -1 + 0.5 * ht * (2*k-1)
            R_for_grin = result_wrapper.get()
            E0 = np.exp(-gamma_*R_for_grin)
            yukawa_inhomogenity += 0.5 * (E0 / R_for_grin) * ht
        yukawa_disturbance_martix[i][j] = yukawa_inhomogenity
        yukawa_disturbance_martix[j][i] = yukawa_inhomogenity
    print(j)
    print("--- %s seconds ---" % (time.time() - start_time))




for j in range(r_step):
    r_j = R[j]
    for i in range(j):
        r_i = R[i]
        yukawa_inhomogenity = 0
        for k in range(1, t_step):
            tk = -1 + 0.5 * ht * (2*k-1)
            R_for_grin = np.sqrt(r_i*r_i + r_j*r_j - 2*r_i*r_j*tk)
            E0 = np.exp(-gamma_*R_for_grin)
            yukawa_inhomogenity += 0.5 * (E0 / R_for_grin) * ht
        yukawa_disturbance_martix[i][j] = yukawa_inhomogenity
        yukawa_disturbance_martix[j][i] = yukawa_inhomogenity
    print(j)
    print("--- %s seconds ---" % (time.time() - start_time))

print(yukawa_disturbance_martix[:][:])

# # начинаем итерации

# iteration = 0
# # c
# # 
# eigen_energy = np.complex(0,0)
# # c
# #       do i = 0, nr-1
# #         ri = rr(i)
# #         s1 = DEXPM(-gamma*ri) / ri
# #         psi1(i) = cr * s1
# #       enddo
# #       psi1(nr) = cz
# # c
# #       psi1n = FNORM2(nr,psi1,rr,hr)
print("--- %s seconds ---" % (time.time() - start_time))