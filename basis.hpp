#pragma once

#include <iostream>
#include <math.h>
#include <complex>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

class Basis_function {

    public:

    vector<float> contraction_;
    vector<float> exponent_;
    vector<float> coordinates_;
    vector<float> integral_exponents_ = {0,0,0};
    vector<float> gaussian_norms_;
    int marker_;
    float normalisation_ = 0.0;


    Basis_function();
    void normalize();
    Basis_function(string orbital, vector<float> coordinates, float integral_ex, int marker);
    Basis_function(float contraction, float exponent, vector<float> coordinates, vector<float> integral_exponents_, float normalisation);
    void Reset(float contraction, float exponent, vector<float> coordinates, vector<float> integral_exponents_, float normalisation);
    bool operator< (Basis_function func);
    void add_function(float contraction, float exponent);
    float value(float x, float y, float z);
    ~Basis_function();

    // Attributes
    // ----------
    // contraction : float
    // exponent : float
    // coordinates : Tuple[float, float, float]
    // integral_exponents : Tuple[int, int, int]
    // normalisation_memo : {None, float}
};

vector<float> gaussian_product_coordinate(float exp_1, vector<float> coordinates_1, float exp_2, vector<float> coordinates_2);
float orbital_overlap(float exp_1, vector<float> coord_1, vector<float> integral_exp_1, float exp_2, vector<float> coord_2, vector<float> integral_exp_2);
vector<Basis_function> del_operator(float exponent, vector<float> coord, vector<float> integral_exp, float normalization);
float nuclear_attraction(float exp_1, vector<float> coord_1, vector<float> integral_exp_1, float exp_2, vector<float> coord_2, vector<float> integral_exp_2, \
                         vector<float> nuclei_coordinates);
float coordinate_distance(vector<float> r_1, vector<float> r_2);
float boys_function(float v, float x);

