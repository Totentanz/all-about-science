#pragma once

#include <iostream>
#include <math.h>
#include <complex>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <chrono>
#include "basis.hpp"
#include "head-gordon-pople.hpp"
#include <eigen3/Eigen/Dense>
#include <tbb/task_group.h> 
#include <tbb/task_arena.h>
#include "exchange_corellation.hpp"
// #include <eigen3/Eigen/CXX11/Tensor>


using namespace std;

class Energy {
    public:
    float electron_energy_ = 0.0;
    float correlation_ = 0.0;
    vector<vector<float> > coulomb_law_matrix_;
    vector<vector<float> > orbital_overlap_matrix_;
    vector<vector<float> > kinetic_energy_matrix_;
    vector<vector<float> > nuclear_attraction_matrix_;
    vector<vector<float> > core_hamiltonian_matrix_;
    vector<vector<float> > exchange_corellation_matrix_;
    vector<vector<vector<vector<float> > > > repulsion_tensor_;
    vector<vector<float> > density_matrix_;

    Eigen::MatrixXf coulomb_law_matrix_eigen_;
    Eigen::MatrixXf kinetic_energy_matrix_eigen_;
    Eigen::MatrixXf nuclear_attraction_matrix_eigen_;
    Eigen::MatrixXf orbital_overlap_matrix_eigen_;
    Eigen::MatrixXf core_hamiltonian_matrix_eigen_;
    Eigen::MatrixXf exchange_corellation_matrix_eigen_;
    Eigen::MatrixXf density_matrix_eigen_;
    // Eigen::Tensor<float,4> repulsion_tensor;
    // vector<vector<vector<vector<float> > > > repulsion_tensor_;

    void Set_Coulomb_matrix(vector<map<string, vector<float> > > nuclei_list);
    void Set_Coulomb_matrix_screened(vector<map<string, vector<float> > > nuclei_list);
    void Set_orbital_overlap_matrix(vector<Basis_function> basis_set);
    void Set_kinetic_energy_matrix(vector<Basis_function> basis_set);
    void Set_nuclear_attraction_matrix(vector<Basis_function> basis_set, vector<map<string, vector<float> > > nuclei_list);
    // void Set_core_hamiltonian_matrix_eigen();
    void Set_core_hamiltonian_matrix();
    void Set_repulsion_tensor(vector<Basis_function> basis_set);
    void Set_repulsion_tensor_from_file(vector<Basis_function> basis_set);
    void mock_Set_repulsion_tensor(vector<Basis_function> basis_set);
    void Set_exchange_corellation_matrix(vector<Basis_function> basis_set, vector<vector<float> > density_matrix);
    void mock_Set_exchange_corellation_matrix(vector<Basis_function> basis_set, vector<vector<float> > density_matrix);
    void Set_exchange_corellation_matrix(vector<Basis_function> basis_set, Eigen::MatrixXf density_matrix);
    void Set_density_matrix(vector<vector<float> > orbital_coefficients, int electrons);
    void Set_density_matrix(Eigen::MatrixXf orbital_coefficients, int electrons);
    void Create_density_matrix(int size);
    float get_total_energy(Eigen::MatrixXf hamiltonian);
    float get_total_energy(vector<vector<float> >  hamiltonian) ;
    void get_Fock_matrix(Eigen::MatrixXf Fock_matrix);
    

    ~Energy();

};