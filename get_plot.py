import numpy as np 
import scipy as sp
import matplotlib.pyplot as plt
import sys, os, math as m

name = sys.argv[1]

R = []
WF = []

with open(name, 'r') as fp:
    value = fp.readline()
    i = 0
    while value:
        R.append(i)
        WF.append(float(value))
        i += 1
        value = fp.readline()

plt.plot( R, WF, color='black')
# plt.savefig(name + "_plot.pdf", dpi=300, format='pdf')
plt.show()