import matplotlib.pyplot as plt
import csv
import sys

if len(sys.argv) != 2:
    print('Usage: %s <path-to-image-file> <path-to-config-bundle> <document-type>' % sys.argv[0])
    print('Example: %s ../testdata/image.png ../data-zip/bundle_mrz.zip mrz.*' % sys.argv[0])
    sys.exit(-1)

filepath = sys.argv[1]

X = []
P = []
P2 = []
P3 = []
with open(filepath) as source:
    reader = csv.reader(source)
    for row in reader:
        x = float(row[0])
        X.append(x)
        # print(row[0] + "\t")
        p = float(row[1])
        P.append(p)
        p2 = float(row[2])
        P2.append(p2)
        # p3 = float(row[3])
        # P3.append(p3)
        # print(str(x) + "\t" + str(p))
        
# graph = plt.figure()
plt.plot (X, P, 'b')
plt.plot (X, P2, 'g')
# plt.plot (X, P3, 'r')
plt.xlabel('r, a0')
plt.ylabel('potential, Ry')
plt.ylim(-15, 0)
plt.show()

