#include <iostream>
#include <math.h>
#include <complex>
#include <fstream>
#include <string>

#define R_STEP 1000
#define T_STEP 200

using namespace std;

int alpha = 5;
int Z = 6;
double r_max = 100.42;
int r_step = 100000;
int t_step = 200; // step for cos(teta)
double q_eff = 3.14;
int l = 1;
double epsilon = 0.000000001;
double mixing_koefficient = double(1/double(1024));
double plank2 = 0.29247;
double rho    = 3.7664;
double plank  = sqrt(plank2);
double gamma_  = sqrt(plank)*rho;
double alpha_     = alpha / plank2;
double Z_    = Z / plank2;
double orbital_    = l*(l+1);

double h = r_max/double((r_step + 1));
double ht = 2/double(t_step);

double R[R_STEP] = {0};

complex<double> complex_zero(0,0);
complex<double> ci(0.0,1.0);
complex<double> edge_value(0.0001,0.);
complex<double> eigen_energy(0,0);
complex<double> eigen_energy_updated(0,0);

complex<double> phi1[R_STEP]={0}; //phi1 = np.zeros(r_step, dtype=np.complex_)
complex<double> phi2[R_STEP]={0};

complex<double> cm(0,0);
complex<double> cp(0,0);
double s1 = 0.;
double s2 = 0.;
double full_potential =0.;

complex<double> wave_function_p[R_STEP]={0};
complex<double> wave_function_k[R_STEP]={0};
double wave_function_p_norm = 0;
double wave_function_k_norm = 0;

double undisturbed_pot[R_STEP] = {0};
double yukawa_pot[R_STEP] = {0};
double yukawa_disturbance_matrix[R_STEP][R_STEP] = {0};


double get_R(int i) {
    return 0.5*h*(2*i+1);
}

double get_undisturbed_potential(double r){
   return (-Z_/r);
//    return (- Z_/r + orbital_ / (2*r*r));

}

double get_full_potential(double ri, double rj, string type) {
    double result = 0;
    if (type == "Brandt") {
        result = -Z_/ri + orbital_/(ri*ri) + alpha_*exp(-rho*ri)/ri;
    } else {
        if (type == "Brandt_virial") {
            // result =  orbital_/(ri*ri) + (-Z_/ri + alpha_*exp(-rho*ri)/ri - alpha_*exp(-rho*ri)*rho)/2;
            result = (-Z_/ri + alpha_*exp(-rho*ri)/ri - alpha_*exp(-rho*ri)*rho)/2;
        }
    }
    return result;
}

double get_R_between_vectors(double ri, double rj, double t){
    return ri*ri + rj*rj - 2*ri*rj*t;
}

complex<double> scalar_multiplication(complex<double>* F, complex<double>* wave_f, double* R){
    complex<double> result = 0;
    for (int i = 0; i< R_STEP; i++) {
        result += F[i]*wave_f[i]*R[i]*R[i]*h;
    };
    return result;
}

double get_square_norm(complex<double>* wave_f, double* R) {
    double result = 0;
    for (int i = 0; i< R_STEP; i++) {
        double absolute = abs(wave_f[i]);
        // cout << "absolute " << absolute << endl;
        result += abs(wave_f[i])*abs(wave_f[i])*R[i]*R[i]*h;
        // cout << "abs(wave_f[i]) " << abs(wave_f[i]) << " R[i] " << R[i] << endl;
    };
    return result;
}

double get_inhomogenity(double ri, double rj) {
    double current_res = 0;
    for (int k = 1; k< T_STEP; k++) {
        double tk = -1 + 0.5*ht*(2*k-1);
        double R_for_grin = sqrt(ri*ri + rj*rj - 2*ri*rj*tk);
        double E0 = exp(-gamma_*R_for_grin);
        // cout << "tk " << tk << " R_for_grin " << R_for_grin << " E0 " << E0 << " ht " << ht << endl;
        current_res += 0.5 * (E0 / R_for_grin) * ht;
    };
    // cout << "current_res " << current_res << endl;
    return current_res;
}

double get_Brandt_inhomogenity(double ri, double rj) {
    return exp(-gamma_*ri)/ri;
}

double get_Brandt_virial_inhomogenity(double ri, double rj) {
    return exp(-gamma_*ri)/(2.0*ri) - exp(-gamma_*ri)*rho/(2.0);
}

double probe_function(double r){
    double kappa = q_eff*r/2;
    // return exp(-kappa/(2))*kappa/(2)/sqrt(24);
    return exp(-kappa/(2))*(kappa/(2)-1)/sqrt(2);

    // return 1/(r_max*r);
}

int main(void) {
    
    // Get coordinates
    for (int i = 0; i< R_STEP; i++) {
        R[i] = get_R(i);
    };
    R[0] = h;
// Get coulomb and orbital pot
    for (int i = 0; i< R_STEP; i++) {
        undisturbed_pot[i] = get_undisturbed_potential(R[i]);
        // cout << "undisturbed_pot[i] " << undisturbed_pot[i] << endl;
    };

// Get Yukawa inhomogenity matrix
ofstream yukawa;
yukawa.open("yukawa.dat");
    for (int j = 0; j< R_STEP; j++) {
        double r_j = R[j];
        for (int i = 0; i<= j; i++) {
            double r_i = R[i];
            double yukawa_inhomogenity = get_Brandt_inhomogenity(r_i, r_j);
            yukawa_disturbance_matrix[i][j] = yukawa_inhomogenity;
            yukawa_disturbance_matrix[j][i] = yukawa_inhomogenity;
            
        }
        yukawa << yukawa_disturbance_matrix[j][j] << endl;
        cout << j << endl;
    };
    yukawa.close();
// Iterations:

    int iteration = 0;

    for (int i = 0; i< R_STEP; i++) {
        wave_function_p[i] = probe_function(R[i]);
        // cout << "wave_function_p[i] " << wave_function_p[i] << " probe_function(R[i]) " << probe_function(R[i])<< endl;
    };
    wave_function_p[R_STEP-1] = edge_value;

    wave_function_p_norm = 1/sqrt(get_square_norm(wave_function_p,R));
    // cout << "wave_function_p_norm " << wave_function_p_norm << endl;


    for (int i = 0; i< R_STEP; i++) {
        wave_function_p[i] *= wave_function_p_norm;
        // cout << "new wave_function_p[i]  is " << wave_function_p[i] << endl;
        wave_function_k[i] = wave_function_p[i];
    };

    cout << "wave_function_p_norm is " << wave_function_p_norm << endl;


    ofstream potential;
    potential.open("init_potential.dat");

    for (int i = 0; i< R_STEP; i++) {
        // ri = R[i]
        // double current_result = 0.;
        // for (int j = 0; j< R_STEP; j++) {
        //     // dr = R[j] * R[j] * h;
        //     double wave_f_real = wave_function_p[j].real();
        //     double wave_f_image = wave_function_p[j].imag();
        //     double pj = wave_f_real*wave_f_real + wave_f_image*wave_f_image;
        //     // s1 = yukawa_disturbance_matrix[i][j] * pj
        //     current_result += yukawa_disturbance_matrix[i][j] * pj * R[j] * R[j] * h;
        // }
        // yukawa_pot[i] = alpha_ * current_result;
        yukawa_pot[i] = alpha_ * get_Brandt_virial_inhomogenity(R[i], 0);

        // full_potential = undisturbed_pot[i] + yukawa_pot[i];
        full_potential = -get_full_potential(R[i], 0, "Brandt_virial");

        potential << full_potential << endl;
        
        // full_potential.append(gi)
        // with open("potential_"+ str(iteration) + '.dat', 'w') as fp:
        //     for pot in full_potential:
        //         fp.write(str(float(pot)) + '/n')


        if (i == 0) {
            s1 = 0;
            s2 = (R[i] + 0.5*h) / R[i];
            cm = 0;
            cp = (wave_function_p[i] - wave_function_p[i+1])*s2*s2;
        }
        if (i == R_STEP-1){
            s1 = (R[i] - 0.5*h) / R[i];
            s2 = 0;
            cm = (wave_function_p[i] - wave_function_p[i-1])*s1*s1;
            cp = 0;
        } else {
            s1 = (R[i] - 0.5*h) / R[i];
            s2 = (R[i] + 0.5*h) / R[i];
            cm = (wave_function_p[i] - wave_function_p[i-1])*s1*s1;
            cp = (wave_function_p[i] - wave_function_p[i+1])*s2*s2;
        }
        // cout << "s1 " << s1 << " s2 " << s2 << " cm " << cm << " cp " << cp << endl;
        phi1[i] = cp + cm + wave_function_p[i] *R[i]* full_potential*h*h;
        phi2[i] = phi1[i] - eigen_energy * wave_function_p[i]*R[i];
        // cout << "phi1 " << phi1[i] << " phi2 " << phi2[i] << endl;
    }
    potential.close();

    phi1[r_step-1] = edge_value;
    phi2[r_step-1] = edge_value;

    double F_square_norm = get_square_norm(phi2, R);
    cout << "iteration " << iteration << ", eigen_energy " << eigen_energy << "F_square_norm "<< F_square_norm << endl;

while (abs(F_square_norm) > epsilon) {

    iteration += 1;
    // full_potential = []

    eigen_energy_updated = scalar_multiplication(phi1, wave_function_p, R);
    cout << "eigen_energy_updated " << eigen_energy_updated << endl;

    for (int i = 0; i< R_STEP; i++) {
        wave_function_k[i] = phi1[i] / eigen_energy_updated;
    
    }
    wave_function_k_norm = 1/sqrt(get_square_norm(wave_function_k, R));
    cout << "wave_function_k_norm " << wave_function_k_norm << endl;


    for (int i = 0; i< R_STEP; i++) {
        wave_function_k[i] *= wave_function_k_norm;
    }

    eigen_energy = eigen_energy * (1-mixing_koefficient) + eigen_energy_updated * mixing_koefficient;
    // eigen_energy = eigen_energy_updated;
    cout << "eigen_energy new " << eigen_energy << endl;
    
    
    for (int i = 0; i< R_STEP; i++) {
        wave_function_p[i] = wave_function_p[i]*(1-mixing_koefficient) + wave_function_k[i] * mixing_koefficient;
        // wave_function_p[i] = wave_function_k[i];
    }
    cout << "mixing_koefficient " << mixing_koefficient << endl;

    wave_function_p_norm = 1/sqrt(get_square_norm(wave_function_p, R));

    cout << "wave_function_p_norm is " << 1/wave_function_p_norm << endl;

    if (iteration%1000 ==0 ) {
        ofstream fout;
        fout.open("wave_f_" + to_string(iteration)+".dat");
        for (int i = 0; i< R_STEP; i++) {
            wave_function_p[i] *= wave_function_p_norm;
            fout << wave_function_p[i].real() << endl;
        }
        fout.close();
    } else {
        for (int i = 0; i< R_STEP; i++) {
            wave_function_p[i] *= wave_function_p_norm;
            // fout << wave_function_p[i].real() << endl;;
        }
    }
    ofstream iteration_potential;
    if (iteration%1000 ==0 ) {
        iteration_potential.open("iteration_potential_" + to_string(iteration)+".dat");
    }

    for (int i = 0; i< R_STEP; i++) {
        double cur_result = 0;
        // for (int j = 0; j< R_STEP; j++) {
        //     // dr = R[j] * R[j] * h;
        //     double wave_f_real = wave_function_p[j].real();
        //     double wave_f_image = wave_function_p[j].imag();
        //     double pj = wave_f_real*wave_f_real + wave_f_image*wave_f_image;
        //     // s1 = yukawa_disturbance_matrix[i][j] * pj
        //     cur_result += yukawa_disturbance_matrix[i][j] * pj * R[j] * R[j] * h;
        // }
        // yukawa_pot[i] = alpha_ * cur_result;
        yukawa_pot[i] = alpha_ * get_Brandt_virial_inhomogenity(R[i], 0);

        // full_potential = undisturbed_pot[i] + yukawa_pot[i];
        full_potential = -get_full_potential(R[i], 0, "Brandt_virial");
        if (iteration%1000 ==0 ) {
            iteration_potential << full_potential << endl;
        }
        if (i == 0){
            s2 = (R[i] + 0.5*h) / R[i];
            cm = 0;
            cp = (wave_function_p[i] - wave_function_p[i+1])*s2*s2;
        }
        if (i == R_STEP-1){
            s1 = (R[i] - 0.5*h) / R[i];
            cm = (wave_function_p[i] - wave_function_p[i-1])*s1*s1;
            cp = 0;
        } else {
            s1 = ((R[i] - 0.5*h) / R[i]);
            s2 = (R[i] + 0.5*h) / R[i];
            cm = (wave_function_p[i] - wave_function_p[i-1])*s1*s1;
            cp = (wave_function_p[i] - wave_function_p[i+1])*s2*s2;
        }
        phi1[i] = cp + cm + wave_function_p[i] *R[i]* full_potential * h*h;
        phi2[i] = phi1[i] - eigen_energy * wave_function_p[i]*R[i];
        // cout << "phi1[i] " << phi1[i] << " phi2[i] " << phi2[i] << endl;
    }
    if (iteration%1000 ==0 ) {
        iteration_potential.close();
    }

    phi1[r_step-1] = edge_value;
    phi2[r_step-1] = edge_value;

    F_square_norm = get_square_norm(phi2, R);
    cout << "iteration " << iteration << ", eigen_energy " << eigen_energy << "F_square_norm "<< F_square_norm << endl;

}





    cout << "lollissimo" << ci.imag() << endl;
return 0;
}