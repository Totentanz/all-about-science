import numpy as np 
import scipy as sp
import matplotlib.pyplot as plt
import sys, os, math as m
from multiprocessing import Pool
import multiprocessing
import itertools

# A = ('a1', 'b1', 'c1')
# B = ('a2', 'b2', 'c2')
# C = ('a3', 'b3', 'c3')
# D = ('a4', 'b4', 'c4')
# SUM = A +B+C+D

# i = 0

# for g1, g2, g3, g4 in itertools.product(A, B, C, D):
#     print( g1, g2, g3, g4)
#     i += 1
#     print(i)

# def calculate_integral (a,b,c,d):
#     return a + b*2 + c*3 + d*4

# def sort_index(a, b, c, d):
#     if a > b:
#         a, b = b, a
#     if c > d:
#         c, d = d, c
#     if a > c:
#         a, c = c, a
#         b, d = d, b
#     if a == c and b > d:
#         a, c = c, a
#         b, d = d, b
#     return a, b, c, d


# keys = []
# for a, b, c, d in itertools.product(range(30), repeat=4):
#     if not (a > b or c > d or a > c or (a == c and b > d)):
#         keys.append((a, b, c, d))

# if self.processes > 1:
#     pool = Pool(self.processes)
#     values = pool.starmap(self.calculate_integral, keys)
#     pool.close()
#     repulsion_dictionary = dict(zip(keys, values))
# else:
# repulsion_dictionary = {index: calculate_integral(*index) for index in keys}

# repulsion_matrix = np.zeros((30, 30, 30, 30))
# for a, b, c, d in itertools.product(range(30), repeat=4):
#     print((a, b, c, d), ' set to ',sort_index(a, b, c, d))
#     repulsion_matrix.itemset((a, b, c, d), repulsion_dictionary[sort_index(a, b, c, d)])

# def Lol(q,w,e,r,t,y):
#     return q+w+e+r+t+y
# result = multiprocessing.Value('f')
# with Pool(processes=10) as pool:
#     for q in range (10000):
#         result.value += pool.apply(Lol, args=(q, 0.1,0.1,0.1,0.1,0.1))
# print(result.value)

electrons = np.zeros((3,3))
k = 1
for i in range (3) :
    for j in range(3):
        electrons[i][j] = i+j


print (electrons[:][0])
