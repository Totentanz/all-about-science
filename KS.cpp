#include <iostream>
#include <math.h>
#include <complex>
#include <fstream>
#include <string>
#include <vector>
#include "basis.hpp"
#include "nuclear_list.hpp"
#include "energy.hpp"
#include "head-gordon-pople.hpp"
#include <eigen3/Eigen/Eigenvalues>
#include <eigen3/Eigen/Dense>

vector<Basis_function> basis_set;
vector<Basis_function> restricted_basis_set;


int main(void) {
    for (int i = 0; i < nuclei_list.size(); i++){
        // cout << "wattba!\n" << endl;
        Basis_function s1 = Basis_function("P1s", nuclei_list[i]["nuclei_coordinates"], 0, 0);
        Basis_function s2 = Basis_function("P2s", nuclei_list[i]["nuclei_coordinates"], 0,1);
        Basis_function s3 = Basis_function("P2s", nuclei_list[i]["nuclei_coordinates"], 0,1);
        Basis_function p2_x = Basis_function("P2p", nuclei_list[i]["nuclei_coordinates"], 0,2);
        Basis_function p2_y = Basis_function("P2p", nuclei_list[i]["nuclei_coordinates"], 1,3);
        Basis_function p2_z = Basis_function("P2p", nuclei_list[i]["nuclei_coordinates"], 2,4);
        Basis_function p3_x = Basis_function("P3p", nuclei_list[i]["nuclei_coordinates"], 0,2);
        Basis_function p3_y = Basis_function("P3p", nuclei_list[i]["nuclei_coordinates"], 1,3);
        Basis_function p3_z = Basis_function("P3p", nuclei_list[i]["nuclei_coordinates"], 2,4);
        Basis_function d3 = Basis_function("P3d", nuclei_list[i]["nuclei_coordinates"], 2,5);

        s1.normalize();
        s2.normalize();
        p2_x.normalize();
        p2_y.normalize();
        p2_z.normalize();
        s3.normalize();
        d3.normalize();
        p3_x.normalize();
        p3_y.normalize();
        p3_z.normalize();
        basis_set.push_back(s1);
        basis_set.push_back(s2);
        basis_set.push_back(p2_x);
        basis_set.push_back(p2_y);
        basis_set.push_back(p2_z);
        basis_set.push_back(s3);
        basis_set.push_back(p3_x);
        basis_set.push_back(p3_y);
        basis_set.push_back(p3_z);
        basis_set.push_back(d3);


        // Basis_function s1 = Basis_function("1s", nuclei_list[i]["nuclei_coordinates"], 0, 0);
        // Basis_function s2 = Basis_function("2s", nuclei_list[i]["nuclei_coordinates"], 0,1);
        // Basis_function p2_x = Basis_function("2p", nuclei_list[i]["nuclei_coordinates"], 0,2);
        // Basis_function p2_y = Basis_function("2p", nuclei_list[i]["nuclei_coordinates"], 1,3);
        // Basis_function p2_z = Basis_function("2p", nuclei_list[i]["nuclei_coordinates"], 2,4);
        // s1.normalize();
        // s2.normalize();
        // p2_x.normalize();
        // p2_y.normalize();
        // p2_z.normalize();
        // basis_set.push_back(s1);
        // basis_set.push_back(s2);
        // basis_set.push_back(p2_x);
        // basis_set.push_back(p2_y);
        // basis_set.push_back(p2_z);
        restricted_basis_set.push_back(p2_z);
        restricted_basis_set.push_back(p3_z);
        restricted_basis_set.push_back(d3);
        // cout << p2_z.exponent_.size() << endl;

    }
    auto start = std::chrono::system_clock::now();
    Energy energy_obj;
    energy_obj.Set_Coulomb_matrix(nuclei_list);
    // energy_obj.Set_Coulomb_matrix_screened(nuclei_list);
    energy_obj.Set_orbital_overlap_matrix(basis_set);
    energy_obj.Set_kinetic_energy_matrix(basis_set);
    energy_obj.Set_nuclear_attraction_matrix(basis_set, nuclei_list);
    energy_obj.Set_core_hamiltonian_matrix();
    energy_obj.Create_density_matrix(basis_set.size());
    energy_obj.Set_repulsion_tensor(basis_set);
    // energy_obj.mock_Set_repulsion_tensor(basis_set);
    // energy_obj.Set_repulsion_tensor_from_file(basis_set);
    Eigen::EigenSolver<Eigen::MatrixXf > s(energy_obj.core_hamiltonian_matrix_eigen_);
    Eigen::EigenSolver< Eigen::MatrixXf >::EigenvectorsType initial_orbital_coefficients = s.eigenvectors();
    Eigen::MatrixXf initial_guess(basis_set.size(), basis_set.size());
    initial_guess.resize(basis_set.size(), basis_set.size());
    Eigen::MatrixXf Fock_matrix(basis_set.size(), basis_set.size());
    Fock_matrix.resize(basis_set.size(), basis_set.size());
    float prev_total_energy = 0.0;
    float current_total_energy = 0.0;
    float delta_energy = 0.0;
    // cout << initial_orbital_coefficients 
    for (int i = 0; i < basis_set.size(); i++) {
        for (int j = 0; j < basis_set.size(); j++) { 
            initial_guess(i,j) = initial_orbital_coefficients(i,j).real();
        }
    }
    energy_obj.Set_density_matrix(initial_guess, 120);
    energy_obj.Set_exchange_corellation_matrix(basis_set, energy_obj.density_matrix_);
    // energy_obj.mock_Set_exchange_corellation_matrix(restricted_basis_set, energy_obj.density_matrix_);
    energy_obj.get_Fock_matrix(Fock_matrix);
    current_total_energy = energy_obj.get_total_energy(Fock_matrix);
    delta_energy = prev_total_energy-current_total_energy;
    
    cout << initial_orbital_coefficients << endl;
    cout << "core_hamiltonian_matrix_eigen_\n" << energy_obj.core_hamiltonian_matrix_eigen_ << endl;
    cout << basis_set.size() << endl;
    cout << "current_total_energy\t" << current_total_energy << endl;
    int iteration = 1;
    while (abs(delta_energy) > 0.01) {
        prev_total_energy = current_total_energy;
        Eigen::EigenSolver<Eigen::MatrixXf > f(Fock_matrix);
        Eigen::EigenSolver< Eigen::MatrixXf >::EigenvectorsType orbital_coefficients = f.eigenvectors();
        // Eigen::MatrixXf guess(restricted_basis_set.size(), restricted_basis_set.size());    

        for (int i = 0; i < basis_set.size(); i++) {
            for (int j = 0; j < basis_set.size(); j++) { 
                initial_guess(i,j) = orbital_coefficients(i,j).real();
            }
        }

        energy_obj.Set_density_matrix(initial_guess, basis_set.size());
        // energy_obj.mock_Set_exchange_corellation_matrix(restricted_basis_set, energy_obj.density_matrix_);
        energy_obj.Set_exchange_corellation_matrix(basis_set, energy_obj.density_matrix_);

        energy_obj.get_Fock_matrix(Fock_matrix);
        current_total_energy = energy_obj.get_total_energy(Fock_matrix);
        delta_energy = prev_total_energy-current_total_energy;
        cout << "iteration " << iteration << endl;
        // cout << initial_orbital_coefficients << endl;
        // cout << "core_hamiltonian_matrix_eigen_\n" << energy_obj.core_hamiltonian_matrix_eigen_ << endl;
        // cout << restricted_basis_set.size() << endl;
        cout << "current_total_energy\t" << current_total_energy << endl;
        cout << "current orbital energies\n" << f.eigenvalues() << endl;

        iteration++;

    }    
    // energy_obj.Set_repulsion_tensor(basis_set);
    // float repulsion_integral = integrate(basis_set[0],basis_set[7],basis_set[20],basis_set[13]);
    // cout << "repulsion_integral\t" << repulsion_integral << endl;
    // energy_obj.Create_density_matrix(basis_set.size());
    // vector<vector<float> > orbital_coefficients;

    // 


        auto end_element = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed = end_element - start;
        std::cout << "Elapsed time for whole program: " << elapsed.count() << "s" << endl;
        cout << "\nWATTBA!\n" << endl;


}
